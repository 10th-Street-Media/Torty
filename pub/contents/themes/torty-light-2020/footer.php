<?php
/*
 * pub/content/themes/torty-light-2020/footer.php
 *
 * This is the header files for pages using the torty-light-2020 theme.
 *
 * since Torty version 0.1
 *
 */
?>
        </div> <!-- end The Grid -->
    </main> <!-- end The Container -->
    <footer class="w3-container w3-large w3-theme-d1">
        <span class="w3-left w3-padding"><?php echo _("Powered by "); ?><a href="https://codeberg.org/10th-Street-Media/Torty"><?php echo VERSION; ?></a></span>
        <span class="w3-right w3-padding"><?php

    if($open_registration == 1) {
        echo "<a href=\"".$website_url."the-registration.php\">"._('Registration')."</a> | ";
    }

?><a href="<?php echo $website_url; ?>the-login.php"><?php echo _("Login"); ?></a></span>
    </footer>
</body>
</html>
