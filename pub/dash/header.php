<?php
/*
 * pub/dash/header.php
 *
 * This page provides the header code for the dashboard pages.
 *
 * since Torty version 0.1
 *
 */

 if(isset($_COOKIE['loc'])) {
    $user_locale = $_COOKIE['loc'];
 } else {
    $user_locale = "en";
 }

    // have Amore use the right localization
    putenv("LC_MESSAGES=".$user_locale);
    setlocale(LC_MESSAGES, $user_locale);

    // set the textdomain
    $textdomain = "torty";
    bindtextdomain($textdomain, "../locales");
    bind_textdomain_codeset($textdomain, 'UTF-8');
    textdomain($textdomain);
?>
<!DOCTYPE html>
<html lang="<?php echo $user_locale; ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="images/favicon.ico">
    <title><?php echo $pagetitle; ?></title>
    <meta name="description" content="<?php echo $website_description; ?>">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- include libraries(jQuery, bootstrap) -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="style/dash-style-default.css" type="text/css">

</head>
<body>
    <div class="dash-top">
        <header class="dash-container dash-bar dash-large">
            <div class="dash-left dash-padding"><?php echo $website_name; ?></div>
            <div class="dash-right dash-padding"><?php
if ($u_dname != "") {
    echo _("Hello, <a href=\"".$website_url."dash/profile.php\">$u_dname</a>");
} else {
    if ($u_name == "") {
        $u_name = $_COOKIE['uname'];
    }
    echo _("Hello, <a href=\"".$website_url."dash/profile.php\">$u_name</a>");
}
?></div>
            <div class="dash-center dash-padding dash-large"><img src="<?php echo $website_url; ?>images/torty-white-24.png"></div>
        </header>
    </div> <!-- .dash-top -->
