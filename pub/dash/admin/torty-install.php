<?php
/*
 * pub/dash/admin/torty-install.php
 *
 * This page gathers some basic information and installs the website.
 *
 * since Torty version 0.1
 *
 */

include "../../../functions.php";


/**
 *
 * Let us verify that the ../../../conn.php file does or does not exist.
 */

if (file_exists("../../../conn.php")) {

	#
    # We should probably throw some sort of error if conn.php exists
    #


}

if (isset($_POST['startsubmit'])) {

	/**
	 * collect our form data
	 */
	$dbhost	= $_POST['dbhost'];
	$dbname	= $_POST['dbname'];
	$dbuser	= $_POST['dbuser'];
	$dbpass	= $_POST['dbpass'];
	$tblpre	= $_POST['tblprefix'];
	$version = "Torty 0.1";


	if (!isset($message)) {
		/**
		 * now create ../../conn.php
		 */

		$connmeta = fopen("../../conn.php", "x+") or die(error_log("Unable to open or create conn.php file"));

		$conndata = "<?php\n";
		$conndata .= "/*\n";
		$conndata .= " *\n";
		$conndata .= " * conn.php\n";
		$conndata .= " *\n";
		$conndata .= " * Stores information that allows Torty to connect to the database.\n";
		$conndata .= " *\n";
		$conndata .= " * since Torty version 0.1\n";
		$conndata .= " *\n";
		$conndata .= " */\n\n";
		$conndata .= "define(\"DBHOST\",\"".$dbhost."\");\n";
		$conndata .= "define(\"DBNAME\",\"".$dbname."\");\n";
		$conndata .= "define(\"DBUSER\",\"".$dbuser."\");\n";
		$conndata .= "define(\"DBPASS\",\"".$dbpass."\");\n";
		$conndata .= "define(\"TBLPREFIX\",\"".$tblpre."\");\n";
        $conndata .= "define(\"VERSION\",\"".$version."\");\n";


		// let us try to write to it.
		fwrite($connmeta,$conndata);
		fclose($connmeta);


		#include_once "schema.php";
		header("Location: schema.php");
	} // end if !isset $message
}



/**
 * In the future, there will be a language chooser before we get to this point.
 */

$pagetitle              = _("Welcome to Torty");
$u_dname                = "";
$u_name                 = "";
$website_description    = _("Form to collect database connection information to Torty to use.");
$website_name           = _("Torty Installation, Step 1");
include_once "admin-header.php";

?>
	<!-- THE CONTAINER for the main content -->
	<main class="w3-container w3-content" style="max-width:1400px;margin-top:40px;">
        <h1 class="hidden"><?php echo $pagetitle; ?></h1>
        <br>
	<!-- THE GRID -->
		<div class="w3-cell-row w3-container">
			<aside class="w3-col w3-cell m3 l4">
                <span><?php echo _("The database must already have been created."); ?></span><br>
                <span><?php echo _('Passphrase must be at least 16 characters long.'); ?></span><br>
                <span><?php echo _('Passphrase must have:')."\n"; ?></span>
				<ul>
					<li><?php echo _('at least one lowercase letter'); ?></li>
					<li><?php echo _('at least one uppercase letter'); ?></li>
					<li><?php echo _('at least one numeral'); ?></li>
				</ul>
			</aside>
			<div class="w3-col w3-panel w3-cell m6 l4">
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
				<h3><?php echo _("Torty installation"); ?></h3>
				<p><?php echo _("We need some database information before we get started."); ?></p>
				<p>
					<label for="dbhost"><?php echo _("Host"); ?></label>
					<input type="text" name="dbhost" id="dbhost" class="w3-input w3-border w3-margin-bottom" maxlength="255" required aria-required="true" value="localhost" title="<?php echo _("The database host. Do not change this unless you know what you are doing."); ?>">
				</p>
				<p>
					<label for="dbname"><?php echo _("Name"); ?></label>
					<input type="text" name="dbname" id="dbname" class="w3-input w3-border w3-margin-bottom" maxlength="30" required aria-required="true" value="torty" title="<?php echo _("The name of the database the website will use."); ?>">
				</p>
				<p>
					<label for="dbuser"><?php echo _("Username"); ?></label>
					<input type="text" name="dbuser" id="dbuser" class="w3-input w3-border w3-margin-bottom" maxlength="30" required aria-required="true" title="<?php echo _("The username of the database admin."); ?>">
				</p>
				<p>
					<label for="dbpass1"><?php echo _("Passphrase"); ?></label>
					<input type="text" name="dbpass" id="dbpass" class="w3-input w3-border w3-margin-bottom" maxlength="255" required aria-required="true" title="<?php echo _("Passphrase must be at least 16 characters long."); ?>">
				</p>
				<p>
					<label for="dbpass2"><?php echo _("Table prefix"); ?></label>
					<input type="text" name="tblprefix" id="tblprefix" class="w3-input w3-border w3-margin-bottom" maxlength="10" aria-required="false" value="ty_" title="<?php echo _("An optional prefix for the tables"); ?>">
				</p>
				<p>
					<input type="submit" name="startsubmit" id="startsubmit" class="w3-button w3-button-hover w3-block w3-theme-d3 w3-section w3-padding" role="button" value="<?php echo _('START SUBMIT'); ?>">
				</p>
			</form>
			</div>
			<div class="w3-col w3-cell m3 l4">&nbsp;</div> <!-- empty div for the purpose of positioning -->

<?php
include_once "admin-footer.php";
?>
