<?php
/*
 * pub/dash/admin/schema.php
 *
 * This file creates the initial database for all programs using the Torty framework.
 *
 * since Torty version 0.1
 *
 */


/*
 * conn.php may not have been put in its proper spot yet
 */
if (file_exists("../../../conn.php")) {
	include_once  "../../../conn.php";
} else if (file_exists("../../conn.php")) {
	include_once "../../conn.php";
} else die(error_log("Unable to find file conn.php. Have you moved it to the correct directory?"));
include			"../../../functions.php";
require			"../../includes/database-connect.php";


//
// Create the configuration table
//
  $configuration_tbl_comment = _("Table for website configuration.");
  $admin_account_field_comment = _("This will be the first account created.");
  $default_no	= _("The default setting is no.");

  $create_configuration_tbl = "CREATE TABLE ".TBLPREFIX."configuration (
    website_name varchar(50) NOT NULL,
    website_url varchar(255) NOT NULL,
    website_description tinytext NOT NULL,
    default_locale tinytext NOT NULL,
    open_registrations BOOLEAN DEFAULT 0 COMMENT '".$default_no."',
    admin_account varchar(20) NOT NULL COMMENT '".$admin_account_field_comment."',
    admin_email varchar(255) NOT NULL,
    banned_user_names text NOT NULL,
    deleted_user_names text NOT NULL,
    theme_slug varchar(20) NOT NULL
  ) DEFAULT CHARSET=utf8mb4 COMMENT='".$configuration_tbl_comment."'";

  if (mysqli_query($dbconn,$create_configuration_tbl)) {
      /* translators: Do not translate ".TBLPREFIX."configuration in following message */
      echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."configuration</i> successfully created.")."</span><br>\n\n";
      #echo $create_configuration_tbl."<br>\n\n";
      error_log(TBLPREFIX."configuration successfully created");
  } else {
      /* translators: Do not translate ".TBLPREFIX."configuration in following message */
      echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."configuration</i>.")."</span><br>\n\n";
      echo $create_configuration_tbl."<br>\n\n";
      error_log("Could not create table ".TBLPREFIX."configuration");
  }





//
// Create the ".TBLPREFIX."locales table
// Table will be filled as new locales are added
//
  $locales_tbl_comment = _("Table for i18n/l10n locales.");

  $create_locales_tbl = "CREATE TABLE ".TBLPREFIX."locales (
    locale_language varchar(3) NOT NULL,
    locale_country varchar(3) NOT NULL,
    PRIMARY KEY (locale_language,locale_country)
  ) DEFAULT CHARSET=utf8mb4 COMMENT='".$locales_tbl_comment."'";

  if (mysqli_query($dbconn,$create_locales_tbl)) {
    /* translators: Do not translate ".TBLPREFIX."locales in following message */
    echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."locales</i> successfully created.")."</span><br>\n\n";
     # echo $create_locales_tbl."<br>\n\n";
      error_log(TBLPREFIX."locales successfully created");
  } else {
    /* translators: Do not translate ".TBLPREFIX."locales in following message */
    echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."locales</i>.")."</span><br>\n\n";
      echo $create_locales_tbl."<br>\n\n";
      error_log("Could not create table ".TBLPREFIX."locales");
  }





//
// Create the ".TBLPREFIX."places_continents table
// These will serve as the parents of the places_countries items
//
    $continents_tbl_comment = _("Table for continents/regions.");

    $create_continents_tbl = "CREATE TABLE ".TBLPREFIX."places_continents (
        continent_id varchar(2) NOT NULL PRIMARY KEY,
        continent_name tinytext NOT NULL
    ) DEFAULT CHARSET=utf8mb4 COMMENT='".$continents_tbl_comment."'";

    if (mysqli_query($dbconn,$create_continents_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."places_continents in following message */
        echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."places_continents</i> successfully created.")."</span><br>\n\n";
       # echo $create_continents_tbl."<br>\n\n";
        error_log(TBLPREFIX."places_continents successfully created");
    } else {
        /* translators: Do not translate ".TBLPREFIX."places_continents in following message */
        echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."places_continents</i>.")."</span><br>\n\n";
        echo $create_continents_tbl."<br>\n\n";
        error_log("Could not create table ".TBLPREFIX."places_continents");
    }





//
// Create the ".TBLPREFIX."places_countries table
// These are children of places_continents
// and parents of places_states
// adapted from https://github.com/dr5hn/countries-states-cities-database
//
    $countries_tbl_comment = _("Table for countries/nations.");
    $country_id_comment = _("ISO 3166-1 alpha-3 country code");
    $country_parent_comment = _("Can be more than one.");

    $create_countries_tbl = "CREATE TABLE ".TBLPREFIX."places_countries (
        country_id mediumint(8) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        country_name varchar(100) NOT NULL,
        country_iso3 char(3) NULL COMMENT '".$country_id_comment."',
        country_iso2 char(2) NULL,
        country_phonecode varchar(255) NULL,
        country_capital varchar(255) NULL,
        country_parent varchar(8) NOT NULL COMMENT '".$country_parent_comment."',
        country_languages tinytext NOT NULL,
        country_currencies tinytext NOT NULL,
        country_created_at timestamp NULL,
        country_updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        country_flag tinyint(1) NOT NULL DEFAULT '1',
        country_wikiDataId varchar(255) NULL COMMENT 'Rapid API GeoDB Cities'
    ) DEFAULT CHARSET=utf8mb4 COMMENT='".$countries_tbl_comment."'";

    if (mysqli_query($dbconn,$create_countries_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."places_countries in following message */
        echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."places_countries</i> successfully created.")."</span><br>\n\n";
        #echo $create_countries_tbl."<br>\n\n";
        error_log(TBLPREFIX."places_countries successfully created");
    } else {
        /* translators: Do not translate ".TBLPREFIX."places_countries in following message */
        echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."places_countries</i>.")."</span><br>\n\n";
        echo $create_countries_tbl."<br>\n\n";
        error_log("Could not create table ".TBLPREFIX."places_countries");
    }





//
// Create the ".TBLPREFIX."places_states table
// These are children of places_countries
// and parents of places_cities
// adapted from https://github.com/dr5hn/countries-states-cities-database
//
    $states_tbl_comment = _("Table for states, provinces, and subnational divisions.");

    $create_states_tbl = "CREATE TABLE ".TBLPREFIX."places_states (
        state_id mediumint(8) unsigned NOT NULL PRIMARY KEY,
        state_name varchar(255) NOT NULL,
        state_country_id mediumint(8) unsigned NOT NULL,
        state_country_code char(2) NOT NULL,
        state_fips_code varchar(255) DEFAULT NULL,
        state_iso2 varchar(255) DEFAULT NULL,
        state_created_at timestamp NULL DEFAULT NULL,
        state_updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        state_flag tinyint(1) NOT NULL DEFAULT '1',
        state_wikiDataId varchar(255) DEFAULT NULL COMMENT 'Rapid API GeoDB Cities'
    ) DEFAULT CHARSET=utf8mb4 COMMENT='".$states_tbl_comment."'";

    if (mysqli_query($dbconn,$create_states_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."places_states in following message */
        echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."places_states</i> successfully created.")."</span><br>\n\n";
       # echo $create_states_tbl."<br>\n\n";
        error_log(TBLPREFIX."places_states successfully created");
    } else {
        /* translators: Do not translate ".TBLPREFIX."places_states in following message */
        echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."places_states</i>.")."</span><br>\n\n";
        echo $create_states_tbl."<br>\n\n";
        error_log("Could not create table ".TBLPREFIX."places_states");
    }





//
// Create the ".TBLPREFIX."places_cities table
// These are children of places_states
// adapted from https://github.com/dr5hn/countries-states-cities-database
//

    $cities_tbl_comment = _("Table for cities");

    $create_cities_tbl = "CREATE TABLE ".TBLPREFIX."places_cities (
        city_id mediumint(8) unsigned NOT NULL PRIMARY KEY,
        city_name varchar(255) NOT NULL,
        city_state_id mediumint(8) unsigned NOT NULL,
        city_state_code varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
        city_country_id mediumint(8) unsigned NOT NULL,
        city_country_code char(2) NOT NULL,
        city_latitude decimal(10,8) NOT NULL,
        city_longitude decimal(11,8) NOT NULL,
        city_created_at timestamp NOT NULL DEFAULT '2013-12-31 19:31:01',
        city_updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        city_flag tinyint(1) NOT NULL DEFAULT '1',
        city_wikiDataId varchar(255) DEFAULT NULL COMMENT 'Rapid API GeoDB Cities'
    ) DEFAULT CHARSET=utf8mb4 COMMENT='".$cities_tbl_comment."'";

    if (mysqli_query($dbconn,$create_cities_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."places_cities in following message */
        echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."places_cities</i> successfully created.")."</span><br>\n\n";
        #echo $create_cities_tbl."<br>\n\n";
        error_log(TBLPREFIX."places_cities successfully created");
    } else {
        /* translators: Do not translate ".TBLPREFIX."places_cities in following message */
        echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."places_cities</i>.")."</span><br>\n\n";
        echo $create_cities_tbl."<br>\n\n";
        error_log("Could not create table ".TBLPREFIX."places_cities");
    }





//
// Create the ".TBLPREFIX."time_zones table
//

    $tzones_tbl_comment = _("Table for time zones");

    $create_tzones_tbl = "CREATE TABLE ".TBLPREFIX."time_zones (
        time_zone_name varchar(50) NOT NULL PRIMARY KEY,
        time_zone_offset varchar(10),
        time_zone_DST_offset varchar(10)
    ) DEFAULT CHARSET=utf8mb4 COMMENT='".$tzones_tbl_comment."'";

    if (mysqli_query($dbconn,$create_tzones_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."time_zones in following message */
        echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."time_zones</i> successfully created.")."</span><br>\n\n";
        #echo $create_tzones_tbl."<br>\n\n";
        error_log(TBLPREFIX."time_zones successfully created");
    } else {
        /* translators: Do not translate ".TBLPREFIX."time_zones in following message */
        echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."time_zones</i>.")."</span><br>\n\n";
        echo $create_tzones_tbl."<br>\n\n";
        error_log("Could not create table ".TBLPREFIX."time_zones");
    }





//
// Create the ".TBLPREFIX."users table
//
$users_tbl_comment = _("Table for users");
$display_name_field_comment = _("This is the same as the ActivityPub preferredUsername");

  $create_users_tbl = "CREATE TABLE ".TBLPREFIX."users (
    user_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_name varchar(20) NOT NULL,
    user_display_name tinytext NOT NULL COMMENT '".$display_name_field_comment."',
    user_pass tinytext NOT NULL,
    user_email tinytext NOT NULL,
    user_date_of_birth date NOT NULL,
    user_level varchar(30) NOT NULL,
    user_actor_type varchar(30) NOT NULL,
    user_priv_key text NOT NULL,
    user_pub_key text NOT NULL,
    user_avatar tinytext,
    user_locale varchar(10) NOT NULL,
    user_location int(11),
    user_time_zone varchar(50) NOT NULL,
    user_bio tinytext NOT NULL,
    user_suspended_until datetime,
    user_suspended_on datetime,
    user_suspended_by varchar(10),
    user_is_banned BOOLEAN DEFAULT 0,
    user_banned_on datetime,
    user_banned_by varchar(10),
    user_created datetime NOT NULL,
    user_last_login datetime NOT NULL,
    UNIQUE KEY user_name (user_name),
    KEY user_date_of_birth (user_date_of_birth),
    KEY user_level (user_level),
    KEY user_actor_type (user_actor_type),
    KEY user_locale (user_locale),
    KEY user_location (user_location),
    KEY user_time_zone (user_time_zone),
    KEY user_suspended_by (user_suspended_by),
    KEY user_suspended_until (user_suspended_until),
    KEY user_suspended_on (user_suspended_on),
    KEY user_banned_by (user_banned_by),
    KEY user_is_banned (user_is_banned),
    KEY user_banned_on (user_banned_on),
    KEY user_created (user_created),
    KEY user_last_login (user_last_login)
  ) DEFAULT CHARSET=utf8mb4 COMMENT='".$users_tbl_comment."'";

  if (mysqli_query($dbconn,$create_users_tbl)) {
    /* translators: Do not translate ".TBLPREFIX."users in following message */
    echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."users</i> successfully created.")."</span><br>\n\n";
      #echo $create_users_tbl."<br>\n\n";
      error_log(TBLPREFIX."users successfully created");
  } else {
    /* translators: Do not translate ".TBLPREFIX."users in following message */
    echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."users</i>.")."</span><br>\n\n";
      echo $create_users_tbl."<br>\n\n";
      error_log("Could not create table ".TBLPREFIX."users");
  }



//
// Create the ".TBLPREFIX."user_levels table
//

    $user_levels_tbl_comment = _("Table for user levels");

    $create_user_levels_tbl = "CREATE TABLE ".TBLPREFIX."user_levels (
        user_level_name varchar(20) NOT NULL PRIMARY KEY
    ) DEFAULT CHARSET=utf8mb4 COMMENT='".$user_levels_tbl_comment."'";

    if (mysqli_query($dbconn,$create_user_levels_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."user_levels in following message */
        echo _("<span style=\"color:green;\">Table <i>".TBLPREFIX."user_levels</i> successfully created.")."</span><br>\n\n";
        #echo $create_user_levels_tbl."<br>\n\n";
        error_log(TBLPREFIX."user_levels successfully created");
    } else {
        /* translators: Do not translate ".TBLPREFIX."user_levels in following message */
        echo _("<span style=\"color:red;\">Error: Could not create table <i>".TBLPREFIX."user_levels</i>.")."</span><br>\n\n";
        echo $create_user_levels_tbl."<br>\n\n";
        error_log("Could not create table ".TBLPREFIX."user_levels");
    }

/* ************************ FILL TABLES ************************************ */
//
// Fill the configuration table with some default data
//
    $default_website_description = _("Another project created with Torty.");

    $fill_configuration_tbl = "INSERT INTO ".TBLPREFIX."configuration (
                    website_name,
                    website_description,
                    default_locale,
                    theme_slug
                ) VALUES (
                    'Torty',
                    '".$default_website_description."',
                    'en-US',
                    'torty-light-2020'
                )";

    if (mysqli_query($dbconn,$fill_configuration_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."configuration in following message */
        echo _("<span style=\"color:green;\">Default data added to table <i>".TBLPREFIX."configuration</i>.")."</span><br>\n\n";
        #echo $fill_configuration_tbl."<br>\n\n";
        error_log("Default data added to ".TBLPREFIX."configuration");
    } else {
        /* translators: Do not translate ".TBLPREFIX."configuration in following message */
        echo _("<span style=\"color:red;\">Error: Could not add data to table <i>".TBLPREFIX."configuration</i>.")."</span><br>\n\n";
        echo $fill_configuration_tbl."<br>\n\n";
        error_log("Default data could not be added to ".TBLPREFIX."configuration");
    }





//
// Fill the ".TBLPREFIX."locales table with a locale
//
    $fill_locales_tbl = "INSERT INTO ".TBLPREFIX."locales (
                    locale_language,
                    locale_country
                ) VALUES (
                    'en',
                    'US'
                )";

    if (mysqli_query($dbconn,$fill_locales_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."locales in following message */
        echo _("<span style=\"color:green;\">Default data added to table <i>".TBLPREFIX."locales</i>.")."</span><br>\n\n";
        #echo $fill_locales_tbl."<br>\n\n";
        error_log("Default data added to ".TBLPREFIX."locales");
    } else {
        /* translators: Do not translate ".TBLPREFIX."locales in following message */
        echo _("<span style=\"color:red;\">Error: Could not add data to table <i>".TBLPREFIX."locales</i>.")."</span><br>\n\n";
        echo $fill_locales_tbl."<br>\n\n";
        error_log("Default data could not be added to ".TBLPREFIX."locales");
    }





//
// Fill the ".TBLPREFIX."places_continents table with default data
//
    $fill_continents_tbl = "INSERT INTO ".TBLPREFIX."places_continents (
                        continent_id,
                        continent_name
                    ) VALUES
                        ('AF','Africa'),
                        ('AN','Antarctica'),
                        ('AS','Asia'),
                        ('EU','Europe'),
                        ('NA','North America'),
                        ('OC','Oceania'),
                        ('SA','South America')";

if (mysqli_query($dbconn,$fill_continents_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."places_continents in following message */
        echo _("<span style=\"color:green;\">Default data added to table <i>".TBLPREFIX."places_continents</i>.")."</span><br>\n\n";
    #echo $fill_continents_tbl."<br>\n\n";
    error_log("Default data added to ".TBLPREFIX."places_continents");
    } else {
        /* translators: Do not translate ".TBLPREFIX."places_continents in following message */
        echo _("<span style=\"color:red;\">Error: Could not add data to table <i>".TBLPREFIX."places_continents</i>.")."</span><br>\n\n";
    echo $fill_continents_tbl."<br>\n\n";
        error_log("Default data could not be added to ".TBLPREFIX."places_continents");
    }



//
// Fill the ".TBLPREFIX."places_countries table with default data
//
    $fill_countries_tbl = "INSERT INTO ".TBLPREFIX."places_countries (
                    country_id,
                    country_name,
                    country_iso3,
                    country_iso2,
                    country_phonecode,
                    country_capital,
                    country_currencies,
                    country_created_at,
                    country_updated_at,
                    country_flag,
                    country_wikiDataId
                ) VALUES
                    (1, 'Afghanistan', 'AFG', 'AF', '93', 'Kabul', 'AFN', '', '', 1, 'Q889'),
                    (2, 'Aland Islands', 'ALA', 'AX', '+358-18', 'Mariehamn', 'EUR', '', '', 1, NULL),
                    (3, 'Albania', 'ALB', 'AL', '355', 'Tirana', 'ALL', '', '', 1, 'Q222'),
                    (4, 'Algeria', 'DZA', 'DZ', '213', 'Algiers', 'DZD', '', '', 1, 'Q262'),
                    (5, 'American Samoa', 'ASM', 'AS', '+1-684', 'Pago Pago', 'USD', '', '', 1, NULL),
                    (6, 'Andorra', 'AND', 'AD', '376', 'Andorra la Vella', 'EUR', '', '', 1, 'Q228'),
                    (7, 'Angola', 'AGO', 'AO', '244', 'Luanda', 'AOA', '', '', 1, 'Q916'),
                    (8, 'Anguilla', 'AIA', 'AI', '+1-264', 'The Valley', 'XCD', '', '', 1, NULL),
                    (9, 'Antarctica', 'ATA', 'AQ', '', '', '', '', '', 1, NULL),
                    (10, 'Antigua And Barbuda', 'ATG', 'AG', '+1-268', 'St. John\'s', 'XCD', '', '', 1, 'Q781'),
                    (11, 'Argentina', 'ARG', 'AR', '54', 'Buenos Aires', 'ARS', '', '', 1, 'Q414'),
                    (12, 'Armenia', 'ARM', 'AM', '374', 'Yerevan', 'AMD', '', '', 1, 'Q399'),
                    (13, 'Aruba', 'ABW', 'AW', '297', 'Oranjestad', 'AWG', '', '', 1, NULL),
                    (14, 'Australia', 'AUS', 'AU', '61', 'Canberra', 'AUD', '', '', 1, 'Q408'),
                    (15, 'Austria', 'AUT', 'AT', '43', 'Vienna', 'EUR', '', '', 1, 'Q40'),
                    (16, 'Azerbaijan', 'AZE', 'AZ', '994', 'Baku', 'AZN', '', '', 1, 'Q227'),
                    (17, 'Bahamas The', 'BHS', 'BS', '+1-242', 'Nassau', 'BSD', '', '', 1, 'Q778'),
                    (18, 'Bahrain', 'BHR', 'BH', '973', 'Manama', 'BHD', '', '', 1, 'Q398'),
                    (19, 'Bangladesh', 'BGD', 'BD', '880', 'Dhaka', 'BDT', '', '', 1, 'Q902'),
                    (20, 'Barbados', 'BRB', 'BB', '+1-246', 'Bridgetown', 'BBD', '', '', 1, 'Q244'),
                    (21, 'Belarus', 'BLR', 'BY', '375', 'Minsk', 'BYR', '', '', 1, 'Q184'),
                    (22, 'Belgium', 'BEL', 'BE', '32', 'Brussels', 'EUR', '', '', 1, 'Q31'),
                    (23, 'Belize', 'BLZ', 'BZ', '501', 'Belmopan', 'BZD', '', '', 1, 'Q242'),
                    (24, 'Benin', 'BEN', 'BJ', '229', 'Porto-Novo', 'XOF', '', '', 1, 'Q962'),
                    (25, 'Bermuda', 'BMU', 'BM', '+1-441', 'Hamilton', 'BMD', '', '', 1, NULL),
                    (26, 'Bhutan', 'BTN', 'BT', '975', 'Thimphu', 'BTN', '', '', 1, 'Q917'),
                    (27, 'Bolivia', 'BOL', 'BO', '591', 'Sucre', 'BOB', '', '', 1, 'Q750'),
                    (28, 'Bosnia and Herzegovina', 'BIH', 'BA', '387', 'Sarajevo', 'BAM', '', '', 1, 'Q225'),
                    (29, 'Botswana', 'BWA', 'BW', '267', 'Gaborone', 'BWP', '', '', 1, 'Q963'),
                    (30, 'Bouvet Island', 'BVT', 'BV', '', '', 'NOK', '', '', 1, NULL),
                    (31, 'Brazil', 'BRA', 'BR', '55', 'Brasilia', 'BRL', '', '', 1, 'Q155'),
                    (32, 'British Indian Ocean Territory', 'IOT', 'IO', '246', 'Diego Garcia', 'USD', '', '', 1, NULL),
                    (33, 'Brunei', 'BRN', 'BN', '673', 'Bandar Seri Begawan', 'BND', '', '', 1, 'Q921'),
                    (34, 'Bulgaria', 'BGR', 'BG', '359', 'Sofia', 'BGN', '', '', 1, 'Q219'),
                    (35, 'Burkina Faso', 'BFA', 'BF', '226', 'Ouagadougou', 'XOF', '', '', 1, 'Q965'),
                    (36, 'Burundi', 'BDI', 'BI', '257', 'Bujumbura', 'BIF', '', '', 1, 'Q967'),
                    (37, 'Cambodia', 'KHM', 'KH', '855', 'Phnom Penh', 'KHR', '', '', 1, 'Q424'),
                    (38, 'Cameroon', 'CMR', 'CM', '237', 'Yaounde', 'XAF', '', '', 1, 'Q1009'),
                    (39, 'Canada', 'CAN', 'CA', '1', 'Ottawa', 'CAD', '', '', 1, 'Q16'),
                    (40, 'Cape Verde', 'CPV', 'CV', '238', 'Praia', 'CVE', '', '', 1, 'Q1011'),
                    (41, 'Cayman Islands', 'CYM', 'KY', '+1-345', 'George Town', 'KYD', '', '', 1, NULL),
                    (42, 'Central African Republic', 'CAF', 'CF', '236', 'Bangui', 'XAF', '', '', 1, 'Q929'),
                    (43, 'Chad', 'TCD', 'TD', '235', 'N''Djamena', 'XAF', '', '', 1, 'Q657'),
                    (44, 'Chile', 'CHL', 'CL', '56', 'Santiago', 'CLP', '', '', 1, 'Q298'),
                    (45, 'China', 'CHN', 'CN', '86', 'Beijing', 'CNY', '', '', 1, 'Q148'),
                    (46, 'Christmas Island', 'CXR', 'CX', '61', 'Flying Fish Cove', 'AUD', '', '', 1, NULL),
                    (47, 'Cocos (Keeling) Islands', 'CCK', 'CC', '61', 'West Island', 'AUD', '', '', 1, NULL),
                    (48, 'Colombia', 'COL', 'CO', '57', 'Bogota', 'COP', '', '', 1, 'Q739'),
                    (49, 'Comoros', 'COM', 'KM', '269', 'Moroni', 'KMF', '', '', 1, 'Q970'),
                    (50, 'Congo', 'COG', 'CG', '242', 'Brazzaville', 'XAF', '', '', 1, 'Q971'),
                    (51, 'Congo The Democratic Republic Of The', 'COD', 'CD', '243', 'Kinshasa', 'CDF', '', '', 1, 'Q974'),
                    (52, 'Cook Islands', 'COK', 'CK', '682', 'Avarua', 'NZD', '', '', 1, 'Q26988'),
                    (53, 'Costa Rica', 'CRI', 'CR', '506', 'San Jose', 'CRC', '', '', 1, 'Q800'),
                    (54, 'Cote D''Ivoire (Ivory Coast)', 'CIV', 'CI', '225', 'Yamoussoukro', 'XOF', '', '', 1, 'Q1008'),
                    (55, 'Croatia (Hrvatska)', 'HRV', 'HR', '385', 'Zagreb', 'HRK', '', '', 1, 'Q224'),
                    (56, 'Cuba', 'CUB', 'CU', '53', 'Havana', 'CUP', '', '', 1, 'Q241'),
                    (57, 'Cyprus', 'CYP', 'CY', '357', 'Nicosia', 'EUR', '', '', 1, 'Q229'),
                    (58, 'Czech Republic', 'CZE', 'CZ', '420', 'Prague', 'CZK', '', '', 1, 'Q213'),
                    (59, 'Denmark', 'DNK', 'DK', '45', 'Copenhagen', 'DKK', '', '', 1, 'Q35'),
                    (60, 'Djibouti', 'DJI', 'DJ', '253', 'Djibouti', 'DJF', '', '', 1, 'Q977'),
                    (61, 'Dominica', 'DMA', 'DM', '+1-767', 'Roseau', 'XCD', '', '', 1, 'Q784'),
                    (62, 'Dominican Republic', 'DOM', 'DO', '+1-809 and 1-829', 'Santo Domingo', 'DOP', '', '', 1, 'Q786'),
                    (63, 'East Timor', 'TLS', 'TL', '670', 'Dili', 'USD', '', '', 1, 'Q574'),
                    (64, 'Ecuador', 'ECU', 'EC', '593', 'Quito', 'USD', '', '', 1, 'Q736'),
                    (65, 'Egypt', 'EGY', 'EG', '20', 'Cairo', 'EGP', '', '', 1, 'Q79'),
                    (66, 'El Salvador', 'SLV', 'SV', '503', 'San Salvador', 'USD', '', '', 1, 'Q792'),
                    (67, 'Equatorial Guinea', 'GNQ', 'GQ', '240', 'Malabo', 'XAF', '', '', 1, 'Q983'),
                    (68, 'Eritrea', 'ERI', 'ER', '291', 'Asmara', 'ERN', '', '', 1, 'Q986'),
                    (69, 'Estonia', 'EST', 'EE', '372', 'Tallinn', 'EUR', '', '', 1, 'Q191'),
                    (70, 'Ethiopia', 'ETH', 'ET', '251', 'Addis Ababa', 'ETB', '', '', 1, 'Q115'),
                    (71, 'Falkland Islands', 'FLK', 'FK', '500', 'Stanley', 'FKP', '', '', 1, NULL),
                    (72, 'Faroe Islands', 'FRO', 'FO', '298', 'Torshavn', 'DKK', '', '', 1, NULL),
                    (73, 'Fiji Islands', 'FJI', 'FJ', '679', 'Suva', 'FJD', '', '', 1, 'Q712'),
                    (74, 'Finland', 'FIN', 'FI', '358', 'Helsinki', 'EUR', '', '', 1, 'Q33'),
                    (75, 'France', 'FRA', 'FR', '33', 'Paris', 'EUR', '', '', 1, 'Q142'),
                    (76, 'French Guiana', 'GUF', 'GF', '594', 'Cayenne', 'EUR', '', '', 1, NULL),
                    (77, 'French Polynesia', 'PYF', 'PF', '689', 'Papeete', 'XPF', '', '', 1, NULL),
                    (78, 'French Southern Territories', 'ATF', 'TF', '', 'Port-aux-Francais', 'EUR', '', '', 1, NULL),
                    (79, 'Gabon', 'GAB', 'GA', '241', 'Libreville', 'XAF', '', '', 1, 'Q1000'),
                    (80, 'Gambia The', 'GMB', 'GM', '220', 'Banjul', 'GMD', '', '', 1, 'Q1005'),
                    (81, 'Georgia', 'GEO', 'GE', '995', 'Tbilisi', 'GEL', '', '', 1, 'Q230'),
                    (82, 'Germany', 'DEU', 'DE', '49', 'Berlin', 'EUR', '', '', 1, 'Q183'),
                    (83, 'Ghana', 'GHA', 'GH', '233', 'Accra', 'GHS', '', '', 1, 'Q117'),
                    (84, 'Gibraltar', 'GIB', 'GI', '350', 'Gibraltar', 'GIP', '', '', 1, NULL),
                    (85, 'Greece', 'GRC', 'GR', '30', 'Athens', 'EUR', '', '', 1, 'Q41'),
                    (86, 'Greenland', 'GRL', 'GL', '299', 'Nuuk', 'DKK', '', '', 1, NULL),
                    (87, 'Grenada', 'GRD', 'GD', '+1-473', 'St. George''s', 'XCD', '', '', 1, 'Q769'),
                    (88, 'Guadeloupe', 'GLP', 'GP', '590', 'Basse-Terre', 'EUR', '', '', 1, NULL),
                    (89, 'Guam', 'GUM', 'GU', '+1-671', 'Hagatna', 'USD', '', '', 1, NULL),
                    (90, 'Guatemala', 'GTM', 'GT', '502', 'Guatemala City', 'GTQ', '', '', 1, 'Q774'),
                    (91, 'Guernsey and Alderney', 'GGY', 'GG', '+44-1481', 'St Peter Port', 'GBP', '', '', 1, NULL),
                    (92, 'Guinea', 'GIN', 'GN', '224', 'Conakry', 'GNF', '', '', 1, 'Q1006'),
                    (93, 'Guinea-Bissau', 'GNB', 'GW', '245', 'Bissau', 'XOF', '', '', 1, 'Q1007'),
                    (94, 'Guyana', 'GUY', 'GY', '592', 'Georgetown', 'GYD', '', '', 1, 'Q734'),
                    (95, 'Haiti', 'HTI', 'HT', '509', 'Port-au-Prince', 'HTG', '', '', 1, 'Q790'),
                    (96, 'Heard and McDonald Islands', 'HMD', 'HM', ' ', '', 'AUD', '', '', 1, NULL),
                    (97, 'Honduras', 'HND', 'HN', '504', 'Tegucigalpa', 'HNL', '', '', 1, 'Q783'),
                    (98, 'Hong Kong S.A.R.', 'HKG', 'HK', '852', 'Hong Kong', 'HKD', '', '', 1, NULL),
                    (99, 'Hungary', 'HUN', 'HU', '36', 'Budapest', 'HUF', '', '', 1, 'Q28'),
                    (100, 'Iceland', 'ISL', 'IS', '354', 'Reykjavik', 'ISK', '', '', 1, 'Q189'),
                    (101, 'India', 'IND', 'IN', '91', 'New Delhi', 'INR', '', '', 1, 'Q668'),
                    (102, 'Indonesia', 'IDN', 'ID', '62', 'Jakarta', 'IDR', '', '', 1, 'Q252'),
                    (103, 'Iran', 'IRN', 'IR', '98', 'Tehran', 'IRR', '', '', 1, 'Q794'),
                    (104, 'Iraq', 'IRQ', 'IQ', '964', 'Baghdad', 'IQD', '', '', 1, 'Q796'),
                    (105, 'Ireland', 'IRL', 'IE', '353', 'Dublin', 'EUR', '', '', 1, 'Q27'),
                    (106, 'Israel', 'ISR', 'IL', '972', 'Jerusalem', 'ILS', '', '', 1, 'Q801'),
                    (107, 'Italy', 'ITA', 'IT', '39', 'Rome', 'EUR', '', '', 1, 'Q38'),
                    (108, 'Jamaica', 'JAM', 'JM', '+1-876', 'Kingston', 'JMD', '', '', 1, 'Q766'),
                    (109, 'Japan', 'JPN', 'JP', '81', 'Tokyo', 'JPY', '', '', 1, 'Q17'),
                    (110, 'Jersey', 'JEY', 'JE', '+44-1534', 'Saint Helier', 'GBP', '', '', 1, 'Q785'),
                    (111, 'Jordan', 'JOR', 'JO', '962', 'Amman', 'JOD', '', '', 1, 'Q810'),
                    (112, 'Kazakhstan', 'KAZ', 'KZ', '7', 'Astana', 'KZT', '', '', 1, 'Q232'),
                    (113, 'Kenya', 'KEN', 'KE', '254', 'Nairobi', 'KES', '', '', 1, 'Q114'),
                    (114, 'Kiribati', 'KIR', 'KI', '686', 'Tarawa', 'AUD', '', '', 1, 'Q710'),
                    (115, 'Korea North\n', 'PRK', 'KP', '850', 'Pyongyang', 'KPW', '', '', 1, 'Q423'),
                    (116, 'Korea South', 'KOR', 'KR', '82', 'Seoul', 'KRW', '', '', 1, 'Q884'),
                    (117, 'Kuwait', 'KWT', 'KW', '965', 'Kuwait City', 'KWD', '', '', 1, 'Q817'),
                    (118, 'Kyrgyzstan', 'KGZ', 'KG', '996', 'Bishkek', 'KGS', '', '', 1, 'Q813'),
                    (119, 'Laos', 'LAO', 'LA', '856', 'Vientiane', 'LAK', '', '', 1, 'Q819'),
                    (120, 'Latvia', 'LVA', 'LV', '371', 'Riga', 'EUR', '', '', 1, 'Q211'),
                    (121, 'Lebanon', 'LBN', 'LB', '961', 'Beirut', 'LBP', '', '', 1, 'Q822'),
                    (122, 'Lesotho', 'LSO', 'LS', '266', 'Maseru', 'LSL', '', '', 1, 'Q1013'),
                    (123, 'Liberia', 'LBR', 'LR', '231', 'Monrovia', 'LRD', '', '', 1, 'Q1014'),
                    (124, 'Libya', 'LBY', 'LY', '218', 'Tripolis', 'LYD', '', '', 1, 'Q1016'),
                    (125, 'Liechtenstein', 'LIE', 'LI', '423', 'Vaduz', 'CHF', '', '', 1, 'Q347'),
                    (126, 'Lithuania', 'LTU', 'LT', '370', 'Vilnius', 'LTL', '', '', 1, 'Q37'),
                    (127, 'Luxembourg', 'LUX', 'LU', '352', 'Luxembourg', 'EUR', '', '', 1, 'Q32'),
                    (128, 'Macau S.A.R.', 'MAC', 'MO', '853', 'Macao', 'MOP', '', '', 1, NULL),
                    (129, 'Macedonia', 'MKD', 'MK', '389', 'Skopje', 'MKD', '', '', 1, 'Q221'),
                    (130, 'Madagascar', 'MDG', 'MG', '261', 'Antananarivo', 'MGA', '', '', 1, 'Q1019'),
                    (131, 'Malawi', 'MWI', 'MW', '265', 'Lilongwe', 'MWK', '', '', 1, 'Q1020'),
                    (132, 'Malaysia', 'MYS', 'MY', '60', 'Kuala Lumpur', 'MYR', '', '', 1, 'Q833'),
                    (133, 'Maldives', 'MDV', 'MV', '960', 'Male', 'MVR', '', '', 1, 'Q826'),
                    (134, 'Mali', 'MLI', 'ML', '223', 'Bamako', 'XOF', '', '', 1, 'Q912'),
                    (135, 'Malta', 'MLT', 'MT', '356', 'Valletta', 'EUR', '', '', 1, 'Q233'),
                    (136, 'Man (Isle of)', 'IMN', 'IM', '+44-1624', 'Douglas, Isle of Man', 'GBP', '', '', 1, NULL),
                    (137, 'Marshall Islands', 'MHL', 'MH', '692', 'Majuro', 'USD', '', '', 1, 'Q709'),
                    (138, 'Martinique', 'MTQ', 'MQ', '596', 'Fort-de-France', 'EUR', '', '', 1, NULL),
                    (139, 'Mauritania', 'MRT', 'MR', '222', 'Nouakchott', 'MRO', '', '', 1, 'Q1025'),
                    (140, 'Mauritius', 'MUS', 'MU', '230', 'Port Louis', 'MUR', '', '', 1, 'Q1027'),
                    (141, 'Mayotte', 'MYT', 'YT', '262', 'Mamoudzou', 'EUR', '', '', 1, NULL),
                    (142, 'Mexico', 'MEX', 'MX', '52', 'Mexico City', 'MXN', '', '', 1, 'Q96'),
                    (143, 'Micronesia', 'FSM', 'FM', '691', 'Palikir', 'USD', '', '', 1, 'Q702'),
                    (144, 'Moldova', 'MDA', 'MD', '373', 'Chisinau', 'MDL', '', '', 1, 'Q217'),
                    (145, 'Monaco', 'MCO', 'MC', '377', 'Monaco', 'EUR', '', '', 1, NULL),
                    (146, 'Mongolia', 'MNG', 'MN', '976', 'Ulan Bator', 'MNT', '', '', 1, 'Q711'),
                    (147, 'Montenegro', 'MNE', 'ME', '382', 'Podgorica', 'EUR', '', '', 1, 'Q236'),
                    (148, 'Montserrat', 'MSR', 'MS', '+1-664', 'Plymouth', 'XCD', '', '', 1, NULL),
                    (149, 'Morocco', 'MAR', 'MA', '212', 'Rabat', 'MAD', '', '', 1, 'Q1028'),
                    (150, 'Mozambique', 'MOZ', 'MZ', '258', 'Maputo', 'MZN', '', '', 1, 'Q1029'),
                    (151, 'Myanmar', 'MMR', 'MM', '95', 'Nay Pyi Taw', 'MMK', '', '', 1, 'Q836'),
                    (152, 'Namibia', 'NAM', 'NA', '264', 'Windhoek', 'NAD', '', '', 1, 'Q1030'),
                    (153, 'Nauru', 'NRU', 'NR', '674', 'Yaren', 'AUD', '', '', 1, 'Q697'),
                    (154, 'Nepal', 'NPL', 'NP', '977', 'Kathmandu', 'NPR', '', '', 1, 'Q837'),
                    (155, 'Netherlands Antilles', 'ANT', 'AN', '', '', '', '', '', 1, NULL),
                    (156, 'Netherlands The', 'NLD', 'NL', '31', 'Amsterdam', 'EUR', '', '', 1, 'Q55'),
                    (157, 'New Caledonia', 'NCL', 'NC', '687', 'Noumea', 'XPF', '', '', 1, NULL),
                    (158, 'New Zealand', 'NZL', 'NZ', '64', 'Wellington', 'NZD', '', '', 1, 'Q664'),
                    (159, 'Nicaragua', 'NIC', 'NI', '505', 'Managua', 'NIO', '', '', 1, 'Q811'),
                    (160, 'Niger', 'NER', 'NE', '227', 'Niamey', 'XOF', '', '', 1, 'Q1032'),
                    (161, 'Nigeria', 'NGA', 'NG', '234', 'Abuja', 'NGN', '', '', 1, 'Q1033'),
                    (162, 'Niue', 'NIU', 'NU', '683', 'Alofi', 'NZD', '', '', 1, 'Q34020'),
                    (163, 'Norfolk Island', 'NFK', 'NF', '672', 'Kingston', 'AUD', '', '', 1, NULL),
                    (164, 'Northern Mariana Islands', 'MNP', 'MP', '+1-670', 'Saipan', 'USD', '', '', 1, NULL),
                    (165, 'Norway', 'NOR', 'NO', '47', 'Oslo', 'NOK', '', '', 1, 'Q20'),
                    (166, 'Oman', 'OMN', 'OM', '968', 'Muscat', 'OMR', '', '', 1, 'Q842'),
                    (167, 'Pakistan', 'PAK', 'PK', '92', 'Islamabad', 'PKR', '', '', 1, 'Q843'),
                    (168, 'Palau', 'PLW', 'PW', '680', 'Melekeok', 'USD', '', '', 1, 'Q695'),
                    (169, 'Palestinian Territory Occupied', 'PSE', 'PS', '970', 'East Jerusalem', 'ILS', '', '', 1, NULL),
                    (170, 'Panama', 'PAN', 'PA', '507', 'Panama City', 'PAB', '', '', 1, 'Q804'),
                    (171, 'Papua new Guinea', 'PNG', 'PG', '675', 'Port Moresby', 'PGK', '', '', 1, 'Q691'),
                    (172, 'Paraguay', 'PRY', 'PY', '595', 'Asuncion', 'PYG', '', '', 1, 'Q733'),
                    (173, 'Peru', 'PER', 'PE', '51', 'Lima', 'PEN', '', '', 1, 'Q419'),
                    (174, 'Philippines', 'PHL', 'PH', '63', 'Manila', 'PHP', '', '', 1, 'Q928'),
                    (175, 'Pitcairn Island', 'PCN', 'PN', '870', 'Adamstown', 'NZD', '', '', 1, NULL),
                    (176, 'Poland', 'POL', 'PL', '48', 'Warsaw', 'PLN', '', '', 1, 'Q36'),
                    (177, 'Portugal', 'PRT', 'PT', '351', 'Lisbon', 'EUR', '', '', 1, 'Q45'),
                    (178, 'Puerto Rico', 'PRI', 'PR', '+1-787 and 1-939', 'San Juan', 'USD', '', '', 1, NULL),
                    (179, 'Qatar', 'QAT', 'QA', '974', 'Doha', 'QAR', '', '', 1, 'Q846'),
                    (180, 'Reunion', 'REU', 'RE', '262', 'Saint-Denis', 'EUR', '', '', 1, NULL),
                    (181, 'Romania', 'ROU', 'RO', '40', 'Bucharest', 'RON', '', '', 1, 'Q218'),
                    (182, 'Russia', 'RUS', 'RU', '7', 'Moscow', 'RUB', '', '', 1, 'Q159'),
                    (183, 'Rwanda', 'RWA', 'RW', '250', 'Kigali', 'RWF', '', '', 1, 'Q1037'),
                    (184, 'Saint Helena', 'SHN', 'SH', '290', 'Jamestown', 'SHP', '', '', 1, NULL),
                    (185, 'Saint Kitts And Nevis', 'KNA', 'KN', '+1-869', 'Basseterre', 'XCD', '', '', 1, 'Q763'),
                    (186, 'Saint Lucia', 'LCA', 'LC', '+1-758', 'Castries', 'XCD', '', '', 1, 'Q760'),
                    (187, 'Saint Pierre and Miquelon', 'SPM', 'PM', '508', 'Saint-Pierre', 'EUR', '', '', 1, NULL),
                    (188, 'Saint Vincent And The Grenadines', 'VCT', 'VC', '+1-784', 'Kingstown', 'XCD', '', '', 1, 'Q757'),
                    (189, 'Saint-Barthelemy', 'BLM', 'BL', '590', 'Gustavia', 'EUR', '', '', 1, NULL),
                    (190, 'Saint-Martin (French part)', 'MAF', 'MF', '590', 'Marigot', 'EUR', '', '', 1, NULL),
                    (191, 'Samoa', 'WSM', 'WS', '685', 'Apia', 'WST', '', '', 1, 'Q683'),
                    (192, 'San Marino', 'SMR', 'SM', '378', 'San Marino', 'EUR', '', '', 1, 'Q238'),
                    (193, 'Sao Tome and Principe', 'STP', 'ST', '239', 'Sao Tome', 'STD', '', '', 1, 'Q1039'),
                    (194, 'Saudi Arabia', 'SAU', 'SA', '966', 'Riyadh', 'SAR', '', '', 1, 'Q851'),
                    (195, 'Senegal', 'SEN', 'SN', '221', 'Dakar', 'XOF', '', '', 1, 'Q1041'),
                    (196, 'Serbia', 'SRB', 'RS', '381', 'Belgrade', 'RSD', '', '', 1, 'Q403'),
                    (197, 'Seychelles', 'SYC', 'SC', '248', 'Victoria', 'SCR', '', '', 1, 'Q1042'),
                    (198, 'Sierra Leone', 'SLE', 'SL', '232', 'Freetown', 'SLL', '', '', 1, 'Q1044'),
                    (199, 'Singapore', 'SGP', 'SG', '65', 'Singapur', 'SGD', '', '', 1, 'Q334'),
                    (200, 'Slovakia', 'SVK', 'SK', '421', 'Bratislava', 'EUR', '', '', 1, 'Q214'),
                    (201, 'Slovenia', 'SVN', 'SI', '386', 'Ljubljana', 'EUR', '', '', 1, 'Q215'),
                    (202, 'Solomon Islands', 'SLB', 'SB', '677', 'Honiara', 'SBD', '', '', 1, 'Q685'),
                    (203, 'Somalia', 'SOM', 'SO', '252', 'Mogadishu', 'SOS', '', '', 1, 'Q1045'),
                    (204, 'South Africa', 'ZAF', 'ZA', '27', 'Pretoria', 'ZAR', '', '', 1, 'Q258'),
                    (205, 'South Georgia', 'SGS', 'GS', '', 'Grytviken', 'GBP', '', '', 1, NULL),
                    (206, 'South Sudan', 'SSD', 'SS', '211', 'Juba', 'SSP', '', '', 1, 'Q958'),
                    (207, 'Spain', 'ESP', 'ES', '34', 'Madrid', 'EUR', '', '', 1, 'Q29'),
                    (208, 'Sri Lanka', 'LKA', 'LK', '94', 'Colombo', 'LKR', '', '', 1, 'Q854'),
                    (209, 'Sudan', 'SDN', 'SD', '249', 'Khartoum', 'SDG', '', '', 1, 'Q1049'),
                    (210, 'Suriname', 'SUR', 'SR', '597', 'Paramaribo', 'SRD', '', '', 1, 'Q730'),
                    (211, 'Svalbard And Jan Mayen Islands', 'SJM', 'SJ', '47', 'Longyearbyen', 'NOK', '', '', 1, NULL),
                    (212, 'Swaziland', 'SWZ', 'SZ', '268', 'Mbabane', 'SZL', '', '', 1, 'Q1050'),
                    (213, 'Sweden', 'SWE', 'SE', '46', 'Stockholm', 'SEK', '', '', 1, 'Q34'),
                    (214, 'Switzerland', 'CHE', 'CH', '41', 'Berne', 'CHF', '', '', 1, 'Q39'),
                    (215, 'Syria', 'SYR', 'SY', '963', 'Damascus', 'SYP', '', '', 1, 'Q858'),
                    (216, 'Taiwan', 'TWN', 'TW', '886', 'Taipei', 'TWD', '', '', 1, 'Q865'),
                    (217, 'Tajikistan', 'TJK', 'TJ', '992', 'Dushanbe', 'TJS', '', '', 1, 'Q863'),
                    (218, 'Tanzania', 'TZA', 'TZ', '255', 'Dodoma', 'TZS', '', '', 1, 'Q924'),
                    (219, 'Thailand', 'THA', 'TH', '66', 'Bangkok', 'THB', '', '', 1, 'Q869'),
                    (220, 'Togo', 'TGO', 'TG', '228', 'Lome', 'XOF', '', '', 1, 'Q945'),
                    (221, 'Tokelau', 'TKL', 'TK', '690', '', 'NZD', '', '', 1, NULL),
                    (222, 'Tonga', 'TON', 'TO', '676', 'Nuku''alofa', 'TOP', '', '', 1, 'Q678'),
                    (223, 'Trinidad And Tobago', 'TTO', 'TT', '+1-868', 'Port of Spain', 'TTD', '', '', 1, 'Q754'),
                    (224, 'Tunisia', 'TUN', 'TN', '216', 'Tunis', 'TND', '', '', 1, 'Q948'),
                    (225, 'Turkey', 'TUR', 'TR', '90', 'Ankara', 'TRY', '', '', 1, 'Q43'),
                    (226, 'Turkmenistan', 'TKM', 'TM', '993', 'Ashgabat', 'TMT', '', '', 1, 'Q874'),
                    (227, 'Turks And Caicos Islands', 'TCA', 'TC', '+1-649', 'Cockburn Town', 'USD', '', '', 1, NULL),
                    (228, 'Tuvalu', 'TUV', 'TV', '688', 'Funafuti', 'AUD', '', '', 1, 'Q672'),
                    (229, 'Uganda', 'UGA', 'UG', '256', 'Kampala', 'UGX', '', '', 1, 'Q1036'),
                    (230, 'Ukraine', 'UKR', 'UA', '380', 'Kiev', 'UAH', '', '', 1, 'Q212'),
                    (231, 'United Arab Emirates', 'ARE', 'AE', '971', 'Abu Dhabi', 'AED', '', '', 1, 'Q878'),
                    (232, 'United Kingdom', 'GBR', 'GB', '44', 'London', 'GBP', '', '', 1, 'Q145'),
                    (233, 'United States', 'USA', 'US', '1', 'Washington', 'USD', '', '', 1, 'Q30'),
                    (234, 'United States Minor Outlying Islands', 'UMI', 'UM', '1', '', 'USD', '', '', 1, NULL),
                    (235, 'Uruguay', 'URY', 'UY', '598', 'Montevideo', 'UYU', '', '', 1, 'Q77'),
                    (236, 'Uzbekistan', 'UZB', 'UZ', '998', 'Tashkent', 'UZS', '', '', 1, 'Q265'),
                    (237, 'Vanuatu', 'VUT', 'VU', '678', 'Port Vila', 'VUV', '', '', 1, 'Q686'),
                    (238, 'Vatican City State (Holy See)', 'VAT', 'VA', '379', 'Vatican City', 'EUR', '', '', 1, 'Q237'),
                    (239, 'Venezuela', 'VEN', 'VE', '58', 'Caracas', 'VEF', '', '', 1, 'Q717'),
                    (240, 'Vietnam', 'VNM', 'VN', '84', 'Hanoi', 'VND', '', '', 1, 'Q881'),
                    (241, 'Virgin Islands (British)', 'VGB', 'VG', '+1-284', 'Road Town', 'USD', '', '', 1, NULL),
                    (242, 'Virgin Islands (US)', 'VIR', 'VI', '+1-340', 'Charlotte Amalie', 'USD', '', '', 1, NULL),
                    (243, 'Wallis And Futuna Islands', 'WLF', 'WF', '681', 'Mata Utu', 'XPF', '', '', 1, NULL),
                    (244, 'Western Sahara', 'ESH', 'EH', '212', 'El-Aaiun', 'MAD', '', '', 1, NULL),
                    (245, 'Yemen', 'YEM', 'YE', '967', 'Sanaa', 'YER', '', '', 1, 'Q805'),
                    (246, 'Zambia', 'ZMB', 'ZM', '260', 'Lusaka', 'ZMK', '', '', 1, 'Q953'),
                    (247, 'Zimbabwe', 'ZWE', 'ZW', '263', 'Harare', 'ZWL', '', '', 1, 'Q954')";

if (mysqli_query($dbconn,$fill_countries_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."places_countries in following message */
        echo _("<span style=\"color:green;\">Default data added to table <i>".TBLPREFIX."places_countries</i>.")."</span><br>\n\n";
        #echo $fill_countries_tbl."<br>\n\n";
        error_log("Default data added to ".TBLPREFIX."places_countries");
    } else {
        /* translators: Do not translate ".TBLPREFIX."places_countries in following message */
        echo _("<span style=\"color:red;\">Error: Could not add data to table <i>".TBLPREFIX."places_countries</i>.")."</span><br>\n\n";
    echo $fill_countries_tbl."<br>\n\n";
        error_log("Default data could not be added to ".TBLPREFIX."places_countries");
    }



//
// Fill the ".TBLPREFIX."time_zones table with some default data
//
  $fill_time_zones_tbl = "INSERT INTO ".TBLPREFIX."time_zones (
                  time_zone_name,
                  time_zone_offset,
                  time_zone_dst_offset
                ) VALUES
                ('Africa/Casablanca', '+01:00', '+01:00'),
                ('Asia/Dhaka', '+06:00', '+06:00'),
                ('Asia/Macao', '+08:00', '+08:00'),
                ('America/Cayenne', '-03:00', '-03:00'),
                ('Europe/Vilnius', '+02:00', '+03:00'),
                ('Europe/Moscow', '+03:00', '+03:00'),
                ('Africa/Ceuta', '+01:00', '+02:00'),
                ('America/Antigua', '-04:00', '-04:00'),
                ('Australia/Darwin', '+09:30', '+09:30'),
                ('Pacific/Auckland', '+12:00', '+13:00'),
                ('Asia/Manila', '+08:00', '+08:00'),
                ('America/Porto Acre', '-05:00', '-05:00'),
                ('America/Indiana/Indianapolis', '-05:00', '-04:00'),
                ('Europe/Skopje', '+01:00', '+02:00'),
                ('America/Fortaleza', '-03:00', '-03:00'),
                ('Asia/Seoul', '+09:00', '+09:00'),
                ('America/Punta Arenas', '-03:00', '-03:00'),
                ('Africa/Maseru', '+02:00', '+02:00'),
                ('Asia/Kabul', '+04:30', '+04:30'),
                ('Africa/Bissau', '+00:00', '+00:00'),
                ('America/Matamoros', '-06:00', '-05:00'),
                ('Asia/Chongqing', '+08:00', '+08:00'),
                ('Indian/Comoro', '+03:00', '+03:00'),
                ('Indian/Mayotte', '+03:00', '+03:00'),
                ('America/Yakutat', '-09:00', '-08:00'),
                ('America/Miquelon', '-03:00', '-02:00'),
                ('Europe/Zaporozhye', '+02:00', '+03:00'),
                ('America/St Vincent', '-04:00', '-04:00'),
                ('Asia/Yangon', '+06:30', '+06:30'),
                ('Europe/Rome', '+01:00', '+02:00'),
                ('Africa/Lome', '+00:00', '+00:00'),
                ('Asia/Sakhalin', '+11:00', '+11:00'),
                ('America/Mendoza', '-03:00', '-03:00'),
                ('America/Port-au-Prince', '-05:00', '-04:00'),
                ('Asia/Srednekolymsk', '+11:00', '+11:00'),
                ('Europe/Nicosia', '+02:00', '+03:00'),
                ('America/Lima', '-05:00', '-05:00'),
                ('Etc/GMT+1', '-01:00', '-01:00'),
                ('Africa/Blantyre', '+02:00', '+02:00'),
                ('Africa/Conakry', '+00:00', '+00:00'),
                ('America/Santiago', '-04:00', '-03:00'),
                ('Pacific/Guam', '+10:00', '+10:00'),
                ('America/Metlakatla', '-09:00', '-08:00'),
                ('Asia/Phnom Penh', '+07:00', '+07:00'),
                ('Pacific/Pitcairn', '-08:00', '-08:00'),
                ('Europe/Zagreb', '+01:00', '+02:00'),
                ('Pacific/Kwajalein', '+12:00', '+12:00'),
                ('Europe/Oslo', '+01:00', '+02:00'),
                ('Indian/Reunion', '+04:00', '+04:00'),
                ('Europe/Kirov', '+03:00', '+03:00'),
                ('Antarctica/Rothera', '-03:00', '-03:00'),
                ('Africa/Algiers', '+01:00', '+01:00'),
                ('Etc/GMT+9', '-09:00', '-09:00'),
                ('Africa/Lubumbashi', '+02:00', '+02:00'),
                ('America/Jamaica', '-05:00', '-05:00'),
                ('America/Moncton', '-04:00', '-03:00'),
                ('Antarctica/DumontDUrville', '+10:00', '+10:00'),
                ('Etc/UCT', '+00:00', '+00:00'),
                ('Asia/Vientiane', '+07:00', '+07:00'),
                ('Asia/Dacca', '+06:00', '+06:00'),
                ('Africa/Addis Ababa', '+03:00', '+03:00'),
                ('Asia/Brunei', '+08:00', '+08:00'),
                ('America/Nassau', '-05:00', '-04:00'),
                ('America/Havana', '-05:00', '-04:00'),
                ('Asia/Ulan Bator', '+08:00', '+08:00'),
                ('Etc/GMT-10', '+10:00', '+10:00'),
                ('America/Cambridge Bay', '-07:00', '-06:00'),
                ('Pacific/Pohnpei', '+11:00', '+11:00'),
                ('America/Swift Current', '-06:00', '-06:00'),
                ('Etc/GMT0', '+00:00', '+00:00'),
                ('Asia/Rangoon', '+06:30', '+06:30'),
                ('Pacific/Pago Pago', '-11:00', '-11:00'),
                ('Atlantic/Jan Mayen', '+01:00', '+02:00'),
                ('Europe/Zurich', '+01:00', '+02:00'),
                ('Etc/GMT-5', '+05:00', '+05:00'),
                ('Africa/Harare', '+02:00', '+02:00'),
                ('America/Argentina/Salta', '-03:00', '-03:00'),
                ('America/Fort Nelson', '-07:00', '-07:00'),
                ('America/Adak', '-10:00', '-09:00'),
                ('America/Inuvik', '-07:00', '-06:00'),
                ('America/Martinique', '-04:00', '-04:00'),
                ('Pacific/Apia', '+13:00', '+14:00'),
                ('America/Eirunepe', '-05:00', '-05:00'),
                ('Europe/Tirane', '+01:00', '+02:00'),
                ('Asia/Irkutsk', '+08:00', '+08:00'),
                ('America/Grand Turk', '-04:00', '-04:00'),
                ('Europe/Gibraltar', '+01:00', '+02:00'),
                ('Africa/El Aaiun', '+00:00', '+01:00'),
                ('Asia/Baku', '+04:00', '+04:00'),
                ('Europe/Uzhgorod', '+02:00', '+03:00'),
                ('Africa/Luanda', '+01:00', '+01:00'),
                ('America/Santo Domingo', '-04:00', '-04:00'),
                ('America/Montreal', '-05:00', '-04:00'),
                ('Africa/Kinshasa', '+01:00', '+01:00'),
                ('America/Port of Spain', '-04:00', '-04:00'),
                ('America/Anguilla', '-04:00', '-04:00'),
                ('Etc/GMT+6', '-06:00', '-06:00'),
                ('Europe/Kiev', '+02:00', '+03:00'),
                ('America/Indiana/Marengo', '-05:00', '-04:00'),
                ('Asia/Kathmandu', '+05:45', '+05:45'),
                ('Etc/GMT+11', '-11:00', '-11:00'),
                ('Atlantic/Canary', '+00:00', '+01:00'),
                ('America/Rio Branco', '-05:00', '-05:00'),
                ('America/Blanc-Sablon', '-04:00', '-04:00'),
                ('Asia/Katmandu', '+05:45', '+05:45'),
                ('Asia/Aqtau', '+05:00', '+05:00'),
                ('America/Glace Bay', '-04:00', '-03:00'),
                ('Africa/Brazzaville', '+01:00', '+01:00'),
                ('America/Tegucigalpa', '-06:00', '-06:00'),
                ('America/St Johns', '-03:30', '-02:30'),
                ('Atlantic/Azores', '-01:00', '+00:00'),
                ('Etc/GMT-4', '+04:00', '+04:00'),
                ('Pacific/Niue', '-11:00', '-11:00'),
                ('America/St Kitts', '-04:00', '-04:00'),
                ('Australia/Perth', '+08:00', '+08:00'),
                ('Pacific/Kiritimati', '+14:00', '+14:00'),
                ('America/Indianapolis', '-05:00', '-04:00'),
                ('Africa/Mbabane', '+02:00', '+02:00'),
                ('America/Asuncion', '-04:00', '-03:00'),
                ('America/Edmonton', '-07:00', '-06:00'),
                ('America/Yellowknife', '-07:00', '-06:00'),
                ('Europe/Sarajevo', '+01:00', '+02:00'),
                ('Africa/Mogadishu', '+03:00', '+03:00'),
                ('America/Winnipeg', '-06:00', '-05:00'),
                ('Asia/Novokuznetsk', '+07:00', '+07:00'),
                ('America/Vancouver', '-08:00', '-07:00'),
                ('America/Virgin', '-04:00', '-04:00'),
                ('Antarctica/Mawson', '+05:00', '+05:00'),
                ('America/Montevideo', '-03:00', '-03:00'),
                ('Pacific/Ponape', '+11:00', '+11:00'),
                ('Indian/Kerguelen', '+05:00', '+05:00'),
                ('Asia/Bahrain', '+03:00', '+03:00'),
                ('Atlantic/Faroe', '+00:00', '+01:00'),
                ('Asia/Oral', '+05:00', '+05:00'),
                ('America/Ojinaga', '-07:00', '-06:00'),
                ('Pacific/Yap', '+10:00', '+10:00'),
                ('America/Kentucky/Louisville', '-05:00', '-04:00'),
                ('Coordinated Universal Time', '+00:00', '+00:00'),
                ('Asia/Qyzylorda', '+06:00', '+06:00'),
                ('Europe/Malta', '+01:00', '+02:00'),
                ('Asia/Karachi', '+05:00', '+05:00'),
                ('Asia/Anadyr', '+12:00', '+12:00'),
                ('America/Bahia', '-03:00', '-03:00'),
                ('America/Noronha', '-02:00', '-02:00'),
                ('Asia/Pyongyang', '+09:00', '+09:00'),
                ('Asia/Ashkhabad', '+05:00', '+05:00'),
                ('America/Danmarkshavn', '+00:00', '+00:00'),
                ('America/Anchorage', '-09:00', '-08:00'),
                ('America/Buenos Aires', '-03:00', '-03:00'),
                ('America/Boa Vista', '-04:00', '-04:00'),
                ('Etc/GMT-6', '+06:00', '+06:00'),
                ('Europe/Vatican', '+01:00', '+02:00'),
                ('Asia/Tokyo', '+09:00', '+09:00'),
                ('America/Montserrat', '-04:00', '-04:00'),
                ('Antarctica/Casey', '+11:00', '+11:00'),
                ('Europe/Berlin', '+01:00', '+02:00'),
                ('America/Detroit', '-05:00', '-04:00'),
                ('Asia/Bangkok', '+07:00', '+07:00'),
                ('America/Manaus', '-04:00', '-04:00'),
                ('America/Guayaquil', '-05:00', '-05:00'),
                ('America/Sitka', '-09:00', '-08:00'),
                ('America/Argentina/Mendoza', '-03:00', '-03:00'),
                ('America/Ensenada', '-08:00', '-07:00'),
                ('Europe/Ulyanovsk', '+04:00', '+04:00'),
                ('America/Louisville', '-05:00', '-04:00'),
                ('Asia/Tblisi', '+04:00', '+04:00'),
                ('Africa/Monrovia', '+00:00', '+00:00'),
                ('America/La Paz', '-04:00', '-04:00'),
                ('Africa/Lusaka', '+02:00', '+02:00'),
                ('Africa/Bangui', '+01:00', '+01:00'),
                ('America/Araguaina', '-03:00', '-03:00'),
                ('Europe/Athens', '+02:00', '+03:00'),
                ('Asia/Hovd', '+07:00', '+07:00'),
                ('Etc/GMT+0', '+00:00', '+00:00'),
                ('America/Belize', '-06:00', '-06:00'),
                ('Africa/Kampala', '+03:00', '+03:00'),
                ('Asia/Damascus', '+02:00', '+03:00'),
                ('Asia/Chungking', '+08:00', '+08:00'),
                ('Pacific/Truk', '+10:00', '+10:00'),
                ('America/Los Angeles', '-08:00', '-07:00'),
                ('America/Recife', '-03:00', '-03:00'),
                ('America/Indiana/Knox', '-06:00', '-05:00'),
                ('Europe/Stockholm', '+01:00', '+02:00'),
                ('Asia/Jakarta', '+07:00', '+07:00'),
                ('Etc/GMT-14', '+14:00', '+14:00'),
                ('Asia/Beirut', '+02:00', '+03:00'),
                ('Asia/Ashgabat', '+05:00', '+05:00'),
                ('Etc/UTC', '+00:00', '+00:00'),
                ('Asia/Ust-Nera', '+10:00', '+10:00'),
                ('America/Barbados', '-04:00', '-04:00'),
                ('Asia/Magadan', '+11:00', '+11:00'),
                ('America/Dominica', '-04:00', '-04:00'),
                ('America/Dawson Creek', '-07:00', '-07:00'),
                ('Pacific/Majuro', '+12:00', '+12:00'),
                ('Africa/Dar es Salaam', '+03:00', '+03:00'),
                ('Europe/Vienna', '+01:00', '+02:00'),
                ('America/Pangnirtung', '-05:00', '-04:00'),
                ('Asia/Vladivostok', '+10:00', '+10:00'),
                ('Asia/Chita', '+09:00', '+09:00'),
                ('Pacific/Saipan', '+10:00', '+10:00'),
                ('America/Indiana/Vevay', '-05:00', '-04:00'),
                ('America/Kentucky/Monticello', '-05:00', '-04:00'),
                ('Africa/Kigali', '+02:00', '+02:00'),
                ('Pacific/Wallis', '+12:00', '+12:00'),
                ('Europe/Saratov', '+04:00', '+04:00'),
                ('Australia/Yancowinna', '+09:30', '+10:30'),
                ('Asia/Ujung Pandang', '+08:00', '+08:00'),
                ('Europe/Istanbul', '+03:00', '+03:00'),
                ('Asia/Colombo', '+05:30', '+05:30'),
                ('Asia/Choibalsan', '+08:00', '+08:00'),
                ('Pacific/Honolulu', '-10:00', '-10:00'),
                ('Asia/Kuwait', '+03:00', '+03:00'),
                ('Etc/GMT+12', '-12:00', '-12:00'),
                ('Asia/Omsk', '+06:00', '+06:00'),
                ('Asia/Hong Kong', '+08:00', '+08:00'),
                ('Africa/Nouakchott', '+00:00', '+00:00'),
                ('Asia/Macau', '+08:00', '+08:00'),
                ('Asia/Istanbul', '+03:00', '+03:00'),
                ('Pacific/Kosrae', '+11:00', '+11:00'),
                ('Africa/Malabo', '+01:00', '+01:00'),
                ('Europe/Luxembourg', '+01:00', '+02:00'),
                ('Europe/Sofia', '+02:00', '+03:00'),
                ('America/St Thomas', '-04:00', '-04:00'),
                ('America/Boise', '-07:00', '-06:00'),
                ('America/Iqaluit', '-05:00', '-04:00'),
                ('America/Guatemala', '-06:00', '-06:00'),
                ('Asia/Dushanbe', '+05:00', '+05:00'),
                ('America/Cordoba', '-03:00', '-03:00'),
                ('America/Santa Isabel', '-08:00', '-07:00'),
                ('America/Aruba', '-04:00', '-04:00'),
                ('America/Cuiaba', '-04:00', '-03:00'),
                ('America/Bahia Banderas', '-06:00', '-05:00'),
                ('Asia/Kuala Lumpur', '+08:00', '+08:00'),
                ('Asia/Qatar', '+03:00', '+03:00'),
                ('Asia/Tomsk', '+07:00', '+07:00'),
                ('Europe/Ljubljana', '+01:00', '+02:00'),
                ('Asia/Tehran', '+03:30', '+04:30'),
                ('Asia/Barnaul', '+07:00', '+07:00'),
                ('America/North Dakota/Center', '-06:00', '-05:00'),
                ('Etc/GMT-11', '+11:00', '+11:00'),
                ('Europe/Isle of Man', '+00:00', '+01:00'),
                ('Europe/Andorra', '+01:00', '+02:00'),
                ('Europe/Tiraspol', '+02:00', '+03:00'),
                ('Australia/Broken Hill', '+09:30', '+09:30'),
                ('Antarctica/Vostok', '+06:00', '+06:00'),
                ('Europe/Vaduz', '+01:00', '+02:00'),
                ('Africa/Freetown', '+00:00', '+00:00'),
                ('Etc/GMT+8', '-08:00', '-08:00'),
                ('Europe/Bucharest', '+02:00', '+03:00'),
                ('America/Chihuahua', '-07:00', '-06:00'),
                ('America/Whitehorse', '-08:00', '-07:00'),
                ('Asia/Aqtobe', '+05:00', '+05:00'),
                ('America/Indiana/Tell City', '-06:00', '-05:00'),
                ('Asia/Tel Aviv', '+02:00', '+03:00'),
                ('America/Argentina/Jujuy', '-03:00', '-03:00'),
                ('Europe/Jersey', '+00:00', '+01:00'),
                ('Australia/Melbourne', '+10:00', '+11:00'),
                ('Europe/Paris', '+01:00', '+02:00'),
                ('Africa/Banjul', '+00:00', '+00:00'),
                ('Africa/Johannesburg', '+02:00', '+02:00'),
                ('Etc/GMT+7', '-07:00', '-07:00'),
                ('Asia/Yekaterinburg', '+05:00', '+05:00'),
                ('Europe/Belfast', '+00:00', '+01:00'),
                ('Asia/Kashgar', '+06:00', '+06:00'),
                ('America/Indiana/Petersburg', '-05:00', '-04:00'),
                ('America/Hermosillo', '-07:00', '-07:00'),
                ('America/Costa Rica', '-06:00', '-06:00'),
                ('Europe/Madrid', '+01:00', '+02:00'),
                ('Indian/Christmas', '+07:00', '+07:00'),
                ('America/Kralendijk', '-04:00', '-04:00'),
                ('Asia/Harbin', '+08:00', '+08:00'),
                ('Europe/Busingen', '+01:00', '+02:00'),
                ('Pacific/Palau', '+09:00', '+09:00'),
                ('America/Catamarca', '-03:00', '-03:00'),
                ('America/Argentina/ComodRivadavia', '-03:00', '-03:00'),
                ('Pacific/Funafuti', '+12:00', '+12:00'),
                ('Asia/Shanghai', '+08:00', '+08:00'),
                ('Africa/Douala', '+01:00', '+01:00'),
                ('Etc/GMT+5', '-05:00', '-05:00'),
                ('Europe/Belgrade', '+01:00', '+02:00'),
                ('Australia/Eucla', '+08:45', '+08:45'),
                ('Asia/Atyrau', '+05:00', '+05:00'),
                ('America/Merida', '-06:00', '-05:00'),
                ('Pacific/Noumea', '+11:00', '+11:00'),
                ('Europe/London', '+00:00', '+01:00'),
                ('Atlantic/Cape Verde', '-01:00', '-01:00'),
                ('America/Managua', '-06:00', '-06:00'),
                ('America/Porto Velho', '-04:00', '-04:00'),
                ('Africa/Maputo', '+02:00', '+02:00'),
                ('Asia/Ulaanbaatar', '+08:00', '+08:00'),
                ('Asia/Muscat', '+04:00', '+04:00'),
                ('America/Phoenix', '-07:00', '-07:00'),
                ('Atlantic/Reykjavik', '+00:00', '+00:00'),
                ('Indian/Antananarivo', '+03:00', '+03:00'),
                ('Africa/Niamey', '+01:00', '+01:00'),
                ('America/Regina', '-06:00', '-06:00'),
                ('America/Mazatlan', '-07:00', '-06:00'),
                ('America/Marigot', '-04:00', '-04:00'),
                ('Asia/Singapore', '+08:00', '+08:00'),
                ('Africa/Sao Tome', '+01:00', '+01:00'),
                ('Australia/Sydney', '+10:00', '+11:00'),
                ('America/Scoresbysund', '-01:00', '+00:00'),
                ('America/Argentina/Rio Gallegos', '-03:00', '-03:00'),
                ('Africa/Djibouti', '+03:00', '+03:00'),
                ('Europe/Mariehamn', '+02:00', '+03:00'),
                ('Europe/Tallinn', '+02:00', '+03:00'),
                ('America/Argentina/Catamarca', '-03:00', '-03:00'),
                ('America/Grenada', '-04:00', '-04:00'),
                ('America/Menominee', '-06:00', '-05:00'),
                ('Pacific/Marquesas', '-09:30', '-09:30'),
                ('Antarctica/Palmer', '-03:00', '-03:00'),
                ('Europe/Podgorica', '+01:00', '+02:00'),
                ('Pacific/Easter', '-06:00', '-05:00'),
                ('Australia/Hobart', '+10:00', '+11:00'),
                ('Pacific/Tahiti', '-10:00', '-10:00'),
                ('America/Guyana', '-04:00', '-04:00'),
                ('America/Denver', '-07:00', '-06:00'),
                ('Pacific/Chuuk', '+10:00', '+10:00'),
                ('Asia/Kamchatka', '+12:00', '+12:00'),
                ('Europe/Amsterdam', '+01:00', '+02:00'),
                ('Asia/Riyadh', '+03:00', '+03:00'),
                ('Etc/GMT+3', '-03:00', '-03:00'),
                ('America/Atikokan', '-05:00', '-05:00'),
                ('America/Juneau', '-09:00', '-08:00'),
                ('Australia/Brisbane', '+10:00', '+10:00'),
                ('Europe/Kaliningrad', '+02:00', '+02:00'),
                ('Asia/Calcutta', '+05:30', '+05:30'),
                ('Etc/Universal', '+00:00', '+00:00'),
                ('Pacific/Fakaofo', '+13:00', '+13:00'),
                ('Europe/Samara', '+04:00', '+04:00'),
                ('Pacific/Port Moresby', '+10:00', '+10:00'),
                ('Etc/Greenwich', '+00:00', '+00:00'),
                ('Europe/Guernsey', '+00:00', '+01:00'),
                ('Asia/Ho Chi Min', '+07:00', '+07:00'),
                ('Europe/Volgograd', '+04:00', '+04:00'),
                ('Indian/Chagos', '+06:00', '+06:00'),
                ('Antarctica/Troll', '+00:00', '+02:00'),
                ('Asia/Bishkek', '+06:00', '+06:00'),
                ('America/Tortola', '-04:00', '-04:00'),
                ('Europe/Dublin', '+00:00', '+01:00'),
                ('Asia/Yakutsk', '+09:00', '+09:00'),
                ('Asia/Samarkand', '+05:00', '+05:00'),
                ('America/Caracas', '-04:00', '-04:00'),
                ('Europe/Helsinki', '+02:00', '+03:00'),
                ('America/Sao Paulo', '-03:00', '-02:00'),
                ('Asia/Amman', '+02:00', '+03:00'),
                ('Australia/Lord Howe', '+10:30', '+11:00'),
                ('Europe/Monaco', '+01:00', '+02:00'),
                ('America/Nome', '-09:00', '-08:00'),
                ('Atlantic/St Helena', '+00:00', '+00:00'),
                ('America/Argentina/La Rioja', '-03:00', '-03:00'),
                ('America/Curacao', '-04:00', '-04:00'),
                ('Africa/Asmara', '+03:00', '+03:00'),
                ('Pacific/Bougainville', '+11:00', '+11:00'),
                ('America/Toronto', '-05:00', '-04:00'),
                ('America/Rosario', '-03:00', '-03:00'),
                ('America/Argentina/San Luis', '-03:00', '-03:00'),
                ('Europe/Warsaw', '+01:00', '+02:00'),
                ('Atlantic/Madeira', '+00:00', '+01:00'),
                ('Antarctica/Davis', '+07:00', '+07:00'),
                ('Australia/Lindeman', '+10:00', '+10:00'),
                ('America/Tijuana', '-08:00', '-07:00'),
                ('Africa/Porto-Novo', '+01:00', '+01:00'),
                ('Etc/GMT-13', '+13:00', '+13:00'),
                ('Africa/Timbuktu', '+00:00', '+00:00'),
                ('Europe/Lisbon', '+00:00', '+01:00'),
                ('America/Coral Harbour', '-05:00', '-05:00'),
                ('Africa/Tunis', '+01:00', '+01:00'),
                ('America/Argentina/Buenos Aires', '-03:00', '-03:00'),
                ('Etc/GMT-1', '+01:00', '+01:00'),
                ('Etc/GMT', '+00:00', '+00:00'),
                ('America/Argentina/Ushuaia', '-03:00', '-03:00'),
                ('America/Puerto Rico', '-04:00', '-04:00'),
                ('Asia/Pontianak', '+07:00', '+07:00'),
                ('Africa/Lagos', '+01:00', '+01:00'),
                ('America/Fort Wayne', '-05:00', '-04:00'),
                ('Etc/GMT-12', '+12:00', '+12:00'),
                ('Pacific/Guadalcanal', '+11:00', '+11:00'),
                ('Africa/Khartoum', '+02:00', '+02:00'),
                ('America/Campo Grande', '-04:00', '-03:00'),
                ('Arctic/Longyearbyen', '+01:00', '+02:00'),
                ('Pacific/Norfolk', '+11:00', '+11:00'),
                ('Africa/Abidjan', '+00:00', '+00:00'),
                ('America/Indiana/Vincennes', '-05:00', '-04:00'),
                ('America/Knox IN', '-06:00', '-05:00'),
                ('America/Halifax', '-04:00', '-03:00'),
                ('Asia/Khandyga', '+09:00', '+09:00'),
                ('Pacific/Nauru', '+12:00', '+12:00'),
                ('Pacific/Tongatapu', '+13:00', '+14:00'),
                ('Europe/Prague', '+01:00', '+02:00'),
                ('Africa/Accra', '+00:00', '+00:00'),
                ('America/Argentina/San Juan', '-03:00', '-03:00'),
                ('Africa/Windhoek', '+02:00', '+02:00'),
                ('Asia/Almaty', '+06:00', '+06:00'),
                ('America/New York', '-05:00', '-04:00'),
                ('America/Panama', '-05:00', '-05:00'),
                ('America/Argentina/Cordoba', '-03:00', '-03:00'),
                ('America/Bogota', '-05:00', '-05:00'),
                ('America/North Dakota/Beulah', '-06:00', '-05:00'),
                ('Asia/Famagusta', '+02:00', '+02:00'),
                ('America/Nipigon', '-05:00', '-04:00'),
                ('Asia/Gaza', '+02:00', '+03:00'),
                ('Europe/San Marino', '+01:00', '+02:00'),
                ('Etc/GMT-2', '+02:00', '+02:00'),
                ('Etc/GMT-0', '+00:00', '+00:00'),
                ('Africa/Dakar', '+00:00', '+00:00'),
                ('America/Godthab', '-03:00', '-02:00'),
                ('America/Atka', '-10:00', '-09:00'),
                ('Africa/Ndjamena', '+01:00', '+01:00'),
                ('Europe/Minsk', '+03:00', '+03:00'),
                ('Asia/Dubai', '+04:00', '+04:00'),
                ('Etc/GMT-9', '+09:00', '+09:00'),
                ('Etc/Zulu', '+00:00', '+00:00'),
                ('Africa/Nairobi', '+03:00', '+03:00'),
                ('Pacific/Tarawa', '+12:00', '+12:00'),
                ('Africa/Libreville', '+01:00', '+01:00'),
                ('Asia/Jerusalem', '+02:00', '+03:00'),
                ('Pacific/Gambier', '-09:00', '-09:00'),
                ('Atlantic/Faeroe', '+00:00', '+01:00'),
                ('Asia/Saigon', '+07:00', '+07:00'),
                ('Asia/Taipei', '+08:00', '+08:00'),
                ('Europe/Simferopol', '+03:00', '+03:00'),
                ('Asia/Yerevan', '+04:00', '+04:00'),
                ('Etc/GMT-8', '+08:00', '+08:00'),
                ('Pacific/Rarotonga', '-10:00', '-10:00'),
                ('Antarctica/South Pole', '+12:00', '+13:00'),
                ('America/Thunder Bay', '-05:00', '-04:00'),
                ('Europe/Copenhagen', '+01:00', '+02:00'),
                ('Asia/Makassar', '+08:00', '+08:00'),
                ('America/Jujuy', '-03:00', '-03:00'),
                ('America/Guadeloupe', '-04:00', '-04:00'),
                ('Antarctica/Macquarie', '+11:00', '+11:00'),
                ('Pacific/Wake', '+12:00', '+12:00'),
                ('America/Indiana/Winamac', '-05:00', '-04:00'),
                ('Africa/Bamako', '+00:00', '+00:00'),
                ('Pacific/Enderbury', '+13:00', '+13:00'),
                ('Europe/Chisinau', '+02:00', '+03:00'),
                ('America/Rankin Inlet', '-06:00', '-05:00'),
                ('Pacific/Efate', '+11:00', '+11:00'),
                ('Africa/Ouagadougou', '+00:00', '+00:00'),
                ('America/Resolute', '-06:00', '-05:00'),
                ('Europe/Bratislava', '+01:00', '+02:00'),
                ('America/Rainy River', '-06:00', '-05:00'),
                ('Etc/GMT-3', '+03:00', '+03:00'),
                ('Africa/Tripoli', '+02:00', '+02:00'),
                ('Asia/Dili', '+09:00', '+09:00'),
                ('America/Santarem', '-03:00', '-03:00'),
                ('Antarctica/Syowa', '+03:00', '+03:00'),
                ('Asia/Thimphu', '+06:00', '+06:00'),
                ('Australia/Adelaide', '+09:30', '+10:30'),
                ('Asia/Jayapura', '+09:00', '+09:00'),
                ('Asia/Kolkata', '+05:30', '+05:30'),
                ('Asia/Novosibirsk', '+07:00', '+07:00'),
                ('Asia/Krasnoyarsk', '+07:00', '+07:00'),
                ('America/Cancun', '-05:00', '-05:00'),
                ('America/Thule', '-04:00', '-03:00'),
                ('Europe/Riga', '+02:00', '+03:00'),
                ('America/El Salvador', '-06:00', '-06:00'),
                ('Pacific/Samoa', '-11:00', '-11:00'),
                ('Asia/Aden', '+03:00', '+03:00'),
                ('Africa/Bujumbura', '+02:00', '+02:00'),
                ('Asia/Urumqi', '+06:00', '+06:00'),
                ('Indian/Mauritius', '+04:00', '+04:00'),
                ('Asia/Thimbu', '+06:00', '+06:00'),
                ('Greenwich Mean Time', '+00:00', '+00:00'),
                ('America/Goose Bay', '-04:00', '-03:00'),
                ('Atlantic/South Georgia', '-02:00', '-02:00'),
                ('Etc/GMT+4', '-04:00', '-04:00'),
                ('Europe/Astrakhan', '+04:00', '+04:00'),
                ('America/Chicago', '-06:00', '-05:00'),
                ('America/Paramaribo', '-03:00', '-03:00'),
                ('Indian/Cocos', '+06:30', '+06:30'),
                ('Africa/Gaborone', '+02:00', '+02:00'),
                ('Pacific/Fiji', '+12:00', '+13:00'),
                ('Africa/Cairo', '+02:00', '+02:00'),
                ('America/Creston', '-07:00', '-07:00'),
                ('America/North Dakota/New Salem', '-06:00', '-05:00'),
                ('Africa/Juba', '+03:00', '+03:00'),
                ('America/Mexico City', '-06:00', '-05:00'),
                ('America/Dawson', '-08:00', '-07:00'),
                ('Asia/Hebron', '+02:00', '+03:00'),
                ('America/Argentina/Tucuman', '-03:00', '-03:00'),
                ('Pacific/Chatham', '+12:45', '+13:45'),
                ('Atlantic/Stanley', '-03:00', '-03:00'),
                ('Etc/GMT+2', '-02:00', '-02:00'),
                ('America/Shiprock', '-07:00', '-06:00'),
                ('Antarctica/McMurdo', '+12:00', '+13:00'),
                ('America/Monterrey', '-06:00', '-05:00'),
                ('America/Belem', '-03:00', '-03:00'),
                ('Atlantic/Bermuda', '-04:00', '-03:00'),
                ('Europe/Brussels', '+01:00', '+02:00'),
                ('America/St Barthelmy', '-04:00', '-04:00'),
                ('America/Maceio', '-03:00', '-03:00'),
                ('Indian/Mahe', '+04:00', '+04:00'),
                ('Etc/GMT+10', '-10:00', '-10:00'),
                ('Australia/Currie', '+10:00', '+11:00'),
                ('Asia/Tashkent', '+05:00', '+05:00'),
                ('Indian/Maldives', '+05:00', '+05:00'),
                ('Pacific/Galapagos', '-06:00', '-06:00'),
                ('Pacific/Midway', '-11:00', '-11:00'),
                ('Pacific/Johnston', '-10:00', '-10:00'),
                ('America/Lower Princes', '-04:00', '-04:00'),
                ('Etc/GMT-7', '+07:00', '+07:00'),
                ('Asia/Kuching', '+08:00', '+08:00'),
                ('Australia/Canberra', '+10:00', '+11:00'),
                ('Asia/Baghdad', '+03:00', '+03:00')";

  if (mysqli_query($dbconn,$fill_time_zones_tbl)) {
    /* translators: Do not translate ".TBLPREFIX."time_zones in following message */
    echo _("<span style=\"color:green;\">Default data added to table <i>".TBLPREFIX."time_zones</i>.")."</span><br>\n\n";
      error_log("Default data added to ".TBLPREFIX."time_zones");
  } else {
    /* translators: Do not translate time_zones in following message */
    echo _("<span style=\"color:red;\">Error: Could not add data to table <i>".TBLPREFIX."time_zones</i>.")."</span><br>\n\n";
      error_log("Default data could not be added to ".TBLPREFIX."time_zones");
  }




//
// Fill the ".TBLPREFIX."user_levels table with default data
//
    $fill_user_levels_tbl = "INSERT INTO ".TBLPREFIX."user_levels (
                        user_level_name
                    ) VALUES
                        ('ADMINISTRATOR'),
                        ('GUIDE'),
                        ('MODERATOR'),
                        ('TRANSLATOR'),
                        ('USER')";

if (mysqli_query($dbconn,$fill_user_levels_tbl)) {
        /* translators: Do not translate ".TBLPREFIX."user_levels in following message */
        echo _("<span style=\"color:green;\">Default data added to table <i>".TBLPREFIX."user_levels</i>.")."</span><br>\n\n";
    #echo $fill_user_levels_tbl."<br>\n\n";
        error_log("Default data added to ".TBLPREFIX."user_levels");
    } else {
        /* translators: Do not translate ".TBLPREFIX."user_levels in following message */
        echo _("<span style=\"color:red;\">Error: Could not add data to table <i>".TBLPREFIX."user_levels</i>.")."</span><br>\n\n";
        echo $fill_user_levels_tbl."<br>\n\n";
        error_log("Default data could not be added to ".TBLPREFIX."user_levels");
    }

/**
 * APP CREATORS: Replace 'post-install.php' with a PHP file that creates and fills tabes for your app.
 * At the end of that file, link to 'post-install.php' so Torty can finish the installation process.
 *
 * If you've made changes to this file and want to test it,
 * add a # in front of redirect('...') and remove the # in front of echo <a href="...">
 * If you do not need to test the file, leave the lines as they are.
 */
redirect("post-install.php");
#echo "<a href=\"post-install.php\">"._('Next step')."</a>";
