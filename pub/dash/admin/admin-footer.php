<?php
/*
 * pub/dash/admin/admin-footer.php
 *
 * This footer ends the HTML for all pre-installation admin pages in Torty.
 *
 * since Torty version 0.1
 *
 */
?>
		</div> <!-- end The Grid -->
	</main> <!-- end The Container -->
	<footer class="w3-container w3-content w3-padding" role="banner">
		<span class="w3-padding"><?php echo _("Powered by "); ?><a href="https://codeberg.org/10th-Street-Media/Torty">Torty</a></span>
	</footer>
</body>
</html>
