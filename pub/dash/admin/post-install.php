<?php
/*
 * pub/dash/admin/post-install.php
 *
 * This page has instructions on what to do after installing Federama.
 *
 * since Torty version 0.1
 *
 */

/*
 * conn.php may not have been put in its proper spot yet
 */
if (file_exists("../../../conn.php")) {
	include_once  "../../../conn.php";
} else if (file_exists("../../conn.php")) {
	include_once "../../conn.php";
} else die("Unable to find file conn.php. Have you moved it to the correct directory?");
include			"../../../functions.php";
require			"../../includes/database-connect.php";

if (isset($_POST['ty-submit'])) {

	/**
	 * collect our form data
	 */
	$tysite       = nicetext($_POST['ty-sitename']);
	$tysiteurl    = nicetext($_POST['ty-siteurl'])."/";
	$tyuser       = nicetext($_POST['ty-admin']);
	$typass1      = $_POST['ty-pass1'];
	$typass2      = $_POST['ty-pass2'];

/**
 * Time to see if the passphrase works well
 */
	if (isset($typass1)) {
		if (isset($typass2)) {

			// Can the user type the same passphrase twice without typos?
			if ($typass1 !== $typass2) {
				$message	= "PASSPHRASE_MISMATCH";
			}
		}

		// Is the passphrase at least 16 characters long?
		if (strlen($typass1) < 16) {
			$message = "SHORT_PASSPHRASE";

		// Is the passphrase complex?
		} else if (!preg_match("/^(?=\P{Ll}*\p{Ll})(?=\P{Lu}*\p{Lu})(?=\P{N}*\p{N})(?=[\p{L}\p{N}]*[^\p{L}\p{N}])[\s\S]{8,}$/",$typass1)) {
			$message = "NOT_COMPLEX";
		} else {

			// if it gets this far without errors, we're good
			$hash_pass = password_hash($typass1,PASSWORD_DEFAULT);
		}

	} // end if isset $dbpass1

	if (!isset($message)) {
		/**
		 * Create our first user
		 */

		// let's create some keys
		// from the comments on https://www.php.net/manual/en/function.openssl-pkey-new.php
		$keyconfig = array(
    		"digest_alg" => "sha512",
    		"private_key_bits" => 4096,
    		"private_key_type" => OPENSSL_KEYTYPE_RSA,
		);

		// Create the private and public key
		$res = openssl_pkey_new($keyconfig);

		// Extract the private key from $res to $privkey
		openssl_pkey_export($res, $privkey);

		// write the private key to a file outside the web root
		$privmeta = fopen("../../../keys/".$tyuser."-private.pem", "w") or die("Unable to open or create ../../keys/".$tyuser."-private.pem file");
		fwrite($privmeta,$privkey);

		// Extract the public key from $res to $pubkey
		$pubkey = openssl_pkey_get_details($res);
		$pubkey = $pubkey["key"];

		$uid				= makeid($newid);
		$udatecreate	= date('Y-m-d H:i:s');
		$firstuserq		= "INSERT INTO ".TBLPREFIX."users (user_id, user_name, user_pass, user_level, user_actor_type, user_pub_key, user_created, user_last_login) VALUES ('".$uid."', '".$tyuser."', '".$hash_pass."', 'ADMINISTRATOR', 'PERSON', '".$pubkey."', '".$udatecreate."', '".$udatecreate."')";
		$firstadminq	= "UPDATE ".TBLPREFIX."configuration SET website_name='".$tysite."', website_url='".$tysiteurl."', admin_account='".$tyuser."'";

		$message = $firstuserq."<br>\n\n".$firstadminq;
		$firstuserquery		= mysqli_query($dbconn,$firstuserq);
		$firstadminquery	= mysqli_query($dbconn,$firstadminq);

		redirect("final.php");
	} // end if !isset $message
}

$pagetitle              = _("Create admin user");
$u_dname                = "";
$u_name                 = "";
$website_description    = _("A form to collect some initial website configuration information.");
$website_name           = _("Torty Installation, Step 2");
include_once "admin-header.php";
?>

	<!-- THE CONTAINER for the main content -->
	<main class="w3-container w3-content" style="max-width:1400px;margin-top:40px;">
        <h1 class="hidden"><?php echo $pagetitle; ?></h1>
<?php
switch ($message) {
	case "PASSPHRASE_MISMATCH":
		echo _("The passphrases do not match. Please try again.");
		break;
	case "SHORT_PASSPHRASE":
		echo _("The passphrase is too short. Please try again.");
		break;
	case "NOT_COMPLEX":
		echo _("The passphrase is not complex. Please try again.");
		break;
}
?>
	<!-- THE GRID -->
		<div class="w3-cell-row w3-container">
			<div class="w3-col w3-cell m3 l4">
				<aside>
                    <span><?php echo _('Passphrase must be at least 16 characters long.'); ?></span><br>
                    <span><?php echo _('Passphrase must have:')."\n"; ?></span>
					<ul>
						<li><?php echo _('at least one lowercase letter'); ?></li>
						<li><?php echo _('at least one uppercase letter'); ?></li>
						<li><?php echo _('at least one numeral'); ?></li>
						<li><?php echo _('at least one character that is not a number or a letter.'); ?></li>
					</ul>
				</aside>
			</div>
			<div class="w3-col w3-panel w3-cell m6 l4">
				<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
				<h3><?php echo _("We're almost done!"); ?></h3>
				<p>
					<label for "ty-sitename"><?php echo _("Site name"); ?></label>
					<input type="text" name="ty-sitename" id="ty-sitename" class="w3-input w3-border w3-margin-bottom" maxlength="255" required aria-required="true" title="<?php echo _("The name of the website."); ?>">
				</p>
				<p>
					<label for "ty-siteurl"><?php echo _("Site URL"); ?></label>
					<input type="text" name="ty-siteurl" id="ty-siteurl" class="w3-input w3-border w3-margin-bottom" maxlength="50" required aria-required="true" placeholder="https://example.com" title="<?php echo _("The URL of the website."); ?>">
				</p>
				<p>
					<label for "ty-admin"><?php echo _("Username"); ?></label>
					<input type="text" name="ty-admin" id="ty-admin" class="w3-input w3-border w3-margin-bottom" maxlength="30" required aria-required="true" title="<?php echo _("The admin user will be the first user account entered in the database."); ?>">
				</p>
				<p>
					<label for "ty-pass1"><?php echo _("Passphrase"); ?></label>
					<input type="password" name="ty-pass1" id="ty-pass1" class="w3-input w3-border w3-margin-bottom" maxlength="255" required aria-required="true" title="<?php echo _("Passphrase must be at least 16 characters long."); ?>">
				</p>
				<p>
					<label for "ty-pass2"><?php echo _("Verify passphrase"); ?></label>
					<input type="password" name="ty-pass2" id="ty-pass2" class="w3-input w3-border w3-margin-bottom" maxlength="255" required aria-required="true" title="<?php echo _("Verify your passphrase."); ?>">
				</p>
				<p>
					<input type="submit" name="ty-submit" id="ty-submit" class="w3-button w3-button-hover w3-block w3-theme-d3 w3-section w3-padding" role="button" value="<?php echo _('CREATE USER'); ?>">
				</p>
				</form>
			</div>
			<div class="w3-col w3-cell m3 l4">&nbsp;</div> <!-- empty div for the purpose of positioning -->
<?php
include_once "admin-footer.php";
?>
