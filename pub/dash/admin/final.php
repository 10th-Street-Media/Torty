<?php
/*
 * pub/dash/admin/final.php
 *
 * This page is for final installation instructions.
 *
 * since Torty version 0.1
 *
 */

$pagetitle              = _("Final installation instructions");
$u_dname                = "";
$u_name                 = "";
$website_description    = _("Some instructions on what to do after Torty has been installed.");
$website_name           = _("Torty Installation is complete");

include_once "admin-header.php";
?>

	<!-- THE CONTAINER for the main content -->
	<main class="w3-container w3-content" style="max-width:1400px;margin-top:40px;">
        <h1 class="hidden"><?php echo $pagetitle; ?></h1>

	<!-- THE GRID -->
		<div class="w3-cell-row w3-container">
			<div class="w3-col w3-cell m3 l4">

			</div>
			<div class="w3-col w3-panel w3-cell m6 l4">
				<h3><?php echo _("We're done!"); ?></h3>
				<p><?php echo _("Move <code>conn.php</code> from the <code>pub/</code> directory to the <code>torty/</code> directory."); ?></p>
				<p><a href="../../the-login.php" role="link"><?php echo _("Go to login page"); ?></a></p>
			</div>
			<div class="w3-col w3-cell m3 l4">&nbsp;</div> <!-- empty div for the purpose of positioning -->
<?php
include_once "admin-footer.php";
?>
