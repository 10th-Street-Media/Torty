<?php
/*
 * pub/dash/admin/admin-header.php
 *
 * This page provides the header code for the pre-installation admin pages.
 *
 * since Torty version 0.1
 *
 */

 if(isset($_COOKIE['lingua'])) {
	$user_locale = $_COOKIE['lingua'];
 } else {
	$user_locale = "en";
 }

  	// have Amore use the right localization
	putenv("LC_MESSAGES=".$user_locale);
	setlocale(LC_MESSAGES, $user_locale);

	// set the textdomain
	$textdomain = "torty";
	bindtextdomain($textdomain, "../locales");
	bind_textdomain_codeset($textdomain, 'UTF-8');
	textdomain($textdomain);
?>
<!DOCTYPE html>
<html lang="<?php echo $user_locale; ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="shortcut icon" href="../images/favicon.ico">
	<title><?php echo $pagetitle; ?></title>
	<meta name="description" content="<?php echo $website_description; ?>">

    <!-- include FontAwesome icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- include libraries(jQuery, bootstrap) -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- include quill css and js -->
    <link rel="stylesheet" href="../style/quill.core.css" />
    <script type="text/javascript" src="../style/quill.core.css"></script>
    <script type="text/javascript" src="../style/quill.css"></script>

    <!-- include default Torty dashboard CSS -->
    <link rel="stylesheet" href="../style/dash-style-default.css" type="text/css">

</head>
<body class="w3-theme-l5">
	<div class="w3-top">
	<header class="w3-container w3-bar w3-large w3-theme-d1" role="banner">
		<div class="w3-left w3-padding"><?php echo $website_name; ?></div>
		<div class="w3-right w3-padding"><?php
if ($u_dname !== "") {
	echo _("Hello, <a href=\"".$website_url."dash/profile.php\">$u_dname</a>");
} else if ($u_name !== "") {
	echo _("Hello, <a href=\"".$website_url."dash/profile.php\">$u_name</a>");
} else {
    echo _("Hello, Website Owner");
}
?></div>
		<div class="w3-center w3-padding w3-large"><img src="../../images/torty-white-24.png"></div>
	</header>
	</div> <!-- .w3-top -->
