<?php
/*
 * pub/dash/nav.php
 *
 * The main menu for users logged into a Torty website.
 *
 * since Torty version 0.1
 */

?>
    <!-- THE CONTAINER for the main content -->
    <main class="w3-row w3-content" style="max-width:1400px;margin-top:40px;">

        <!-- THE GRID -->
        <div class="w3-cell-row">

            <nav class="w3-sidebar w3-bar-block w3-theme-d2">
                <a href="<?php echo $website_url."dash/index.php"; ?>" class="w3-bar-item w3-button" title="<?php echo _("Your homepage"); ?>"><i class="fa fa-lg fa-home"></i>&nbsp;<?php echo _("Home"); ?></a>
                <a href="#" class="w3-bar-item w3-button" title="<?php echo _("Your profile."); ?>" onclick="myProfile()"><i class="fa fa-lg fa-user" ></i>&nbsp;<?php echo _("Profile"); ?>&nbsp;<i class="fa fa-caret-down w3-right"></i></a>
                    <div id="profileMenu" class="w3-hide w3-white w3-card">
                        <a href="<?php echo $website_url."dash/profile.php"; ?>" class="w3-bar-item w3-button"><?php echo _('View profile'); ?></a>
                        <a href="<?php echo $website_url."dash/import-profile.php"; ?>" class="w3-bar-item w3-button"><?php echo _('Import profile'); ?></a>
                        <a href="<?php echo $website_url."dash/export-profile.php"; ?>" class="w3-bar-item w3-button"><?php echo _('Export profile'); ?></a>
                        <a href="<?php echo $website_url."dash/delete-profile.php"; ?>" class="w3-bar-item w3-button"><?php echo _('Delete profile'); ?></a>
                        <a href="<?php echo $website_url."dash/change-passphrase.php"; ?>" class="w3-bar-item w3-button"><?php echo _('Change passphrase'); ?></a>
                    </div>
<?php
$adminq = "SELECT * FROM ".TBLPREFIX."users WHERE user_id=".$_COOKIE['id'];
$adminquery = mysqli_query($dbconn,$adminq);
while($adminopt = mysqli_fetch_assoc($adminquery)) {
        $level    = $adminopt['user_level'];

        if ($level == 'ADMINISTRATOR') {
            echo "\t\t\t\t<a href=\"#\" class=\"w3-bar-item w3-button\" title=\""._('User list')."\" onclick=\"myUsersMenu()\"><i class=\"fa fa-lg fa-users\"></i>&nbsp;"._('Users')."&nbsp;<i class=\"fa fa-caret-down w3-right\"></i></a>\n";
            echo "\t\t\t\t\t<div id=\"usersMenu\" class=\"w3-hide w3-white w3-card\">\n";
            echo "\t\t\t\t\t\t<a href=\"".$website_url."dash/admin/users.php\" class=\"w3-bar-item w3-button\">"._('All users')."</a>\n";
            echo "\t\t\t\t\t\t<a href=\"".$website_url."dash/admin/add-user.php\" class=\"w3-bar-item w3-button\">"._('Add user')."</a>\n";
            echo "\t\t\t\t\t</div>\n";
            echo "\t\t\t\t<a href=\"#\" class=\"w3-bar-item w3-button\" title=\""._('Administrator dashboard')."\" onclick=\"myAdminMenu()\"><i class=\"fa fa-lg fa-wrench\"></i>&nbsp;"._('Administrators')."&nbsp;<i class=\"fa fa-caret-down w3-right\"></i></a>\n";
            echo "\t\t\t\t\t<div id=\"adminMenu\" class=\"w3-hide w3-white w3-card\">\n";
            echo "\t\t\t\t\t\t<a href=\"".$website_url."dash/admin/configuration.php\" class=\"w3-bar-item w3-button\">"._('Website configuration')."</a>\n";
            echo "\t\t\t\t\t</div>\n";
        }

}
?>
            </nav>
            <script>
            function myProfile() {
              var x = document.getElementById("profileMenu");
              if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
                x.previousElementSibling.className += " w3-theme-d1";
              } else {
                x.className = x.className.replace(" w3-show", "");
                x.previousElementSibling.className =
                x.previousElementSibling.className.replace(" w3-theme-d1", "");
              }
            }

            function myUsersMenu() {
              var x = document.getElementById("usersMenu");
              if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
                x.previousElementSibling.className += " w3-theme-d1";
              } else {
                x.className = x.className.replace(" w3-show", "");
                x.previousElementSibling.className =
                x.previousElementSibling.className.replace(" w3-theme-d1", "");
              }
            }

            function myAdminMenu() {
              var x = document.getElementById("adminMenu");
              if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
                x.previousElementSibling.className += " w3-theme-d1";
              } else {
                x.className = x.className.replace(" w3-show", "");
                x.previousElementSibling.className =
                x.previousElementSibling.className.replace(" w3-theme-d1", "");
              }
            }
            </script>
