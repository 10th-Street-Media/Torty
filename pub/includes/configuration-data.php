<?php
/*
 * pub/includes/configuration-data.php
 *
 * This page is a template to get the website's configuration from the database.
 *
 * since Torty version 0.1
 *
 */
require "database-connect.php";
// let's get the configuration data

$mysiteq = "SELECT * FROM ".TBLPREFIX."configuration";
$mysitequery = mysqli_query($dbconn,$mysiteq);
while ($mysiteopt = mysqli_fetch_assoc($mysitequery)) {
    $website_url                    = $mysiteopt['website_url'];
    $website_name                   = $mysiteopt['website_name'];
    $website_description            = $mysiteopt['website_description'];
    $default_locale                 = $mysiteopt['default_locale'];
    $open_registration              = $mysiteopt['open_registrations'];
    $admin_account                  = $mysiteopt['admin_account'];
    $admin_email                    = $mysiteopt['admin_email'];
    $banned_usernames               = $mysiteopt['banned_user_names'];
    $deleted_usernames              = $mysiteopt['deleted_user_names'];
    $theme_path                     = $mysiteopt['website_url']."contents/themes/".$mysiteopt['theme-slug'];
}
?>
