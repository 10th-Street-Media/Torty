<?php
/*
 * pub/nodeinfo.php
 *
 * Starts/creates files for Nodeinfo.
 *
 * since Torty version 0.1
 *
 */
?>
<?php
include         "../../functions.php";
require         "database-connect.php";
require_once    "configuration-data.php";

/**
 * This section creates the .well-known/nodeinfo file
 * Nodeinfo looks for this to tell it where to find the JSON files
 * with information about this website
 */
$nodeinfometa = fopen("../.well-known/nodeinfo", "w") or die("Unable to open or create nodeinfo file");

$json0 = "{\"links\":[{\"rel\":\"http://nodeinfo.diaspora.software/ns/schema/1.0\",\"href\":\"".$website_url."/nodeinfo/1.0\"},{\"rel\":\"http://nodeinfo.diaspora.software/ns/schema/2.0\",\"href\":\"".$website_url."/nodeinfo/2.0\"}]}";

fwrite($nodeinfometa,$json0);
fclose($nodeinfometa);


/**
 * This section creates the 1.0.jsonld file
 * This file will work with older versions of Nodeinfo
 */
$nodeinfo1 = fopen("../nodeinfo/1.0.jsonld", "w") or die("Unable to open or create nodeinfo 1.0 file");

$json1 = "{\"version\":\"1.0\",\"software\":{\"name\":\"torty\",\"version\":\"v0.1\"},\"protocols\":{\"inbound\":[],\"outbound\":[]},\"services\":{\"inbound\":[],\"outbound\":[]},\"openRegistrations\":".$open.",\"usage\":{\"users\":{\"total\":".user_quantity($users).",\"activeHalfyear\":".users_half_year($sometimes_users).",\"activeMonth\":".users_past_month($active_users)."},\"localPosts\":".post_quantity($posts).",\"localComments\":},\"metadata\":{\"nodeName\":\"".$website_name."\"}}";

fwrite($nodeinfo1,$json1);
fclose($nodeinfo1);


/**
 * This section creates the 2.0.jsonld file
 * This files works with the most current version of Nodeinfo
 */
$nodeinfo2 = fopen("../nodeinfo/2.0.jsonld", "w") or die("Unable to open or create nodeinfo 2.0 file");

$json2 = "{\"version\":\"2.0\",\"software\":{\"name\":\"torty\",\"version\":\"v0.1\"},\"protocols\":{\"inbound\":[],\"outbound\":[]},\"services\":{\"inbound\":[],\"outbound\":[]},\"openRegistrations\":".$open.",\"usage\":{\"users\":{\"total\":".user_quantity($users).",\"activeHalfyear\":".users_half_year($sometimes_users).",\"activeMonth\":".users_past_month($active_users)."},\"localPosts\":".post_quantity($posts).",\"localComments\":},\"metadata\":{\"nodeName\":\"".$website_name."\"}}";

fwrite($nodeinfo2,$json2);
fclose($nodeinfo2);
?>
