<?php
/**
 * pub/index.php
 *
 * This is the main page for Torty and serves several purposes.
 * If Torty is not installed, it triggers installation.
 * If Torty is installed, it displays a login or registration page.
 * since Torty version 0.1
 *
 */

/**
 * If ../conn.php does not exist...
 */
if(!file_exists("../conn.php")) {

    /**
     * ...and conn.php does not exist...
     */
    if (!file_exists("conn.php")) {

        /**
         * redirect user to install page.
         */
        header("Location: dash/admin/torty-install.php");
    } else {

        /**
         * conn.php does exist
         * redirect user to post-install page
         */
        header("Location: dash/admin/post-install.php");
    }
} else {

    /**
     * ../conn.php does exist
     * Let us include it, then verify its constants
     */

    include "../conn.php";

    /**
     * if $global_count === 5 at the end then all global variables are set.
     * if $global_count < 5 then something is missing.
     */

}

include         "../functions.php";
require         "includes/database-connect.php";
require_once    "includes/configuration-data.php";
include_once    "nodeinfo/version.php";

// see if a session is set. If so, redirect them to their dashboard.

if (isset($_COOKIE['nomen'])) {
    if ($_COOKIE['nomen'] != '') {
        $visitortitle = $_COOKIE['nomen'];
    }
} else {
    $visitortitle = _('Guest');
}


$pagetitle = $website_name;
$objdescription = $website_description;

include_once "includes/ty-header.php";
?>
            <div class="w3-col w3-panel w3-cell m10">

            </div>
<?php
include_once "includes/ty-footer.php";
?>
