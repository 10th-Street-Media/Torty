# Torty

**Torty** is a PHP framework currently under development. It is intended for use with MariaDB or MySQL as the back end database, and will run well with Apache and nginx web servers. As a framework, it is meant to be the basis for independent software projects. It will be well-documented, and there will be project examples to help other developers understand how it works and how it can be extended.
