# Torty Documentation
*This documentation applies to Torty v0.1*

### INSTALLING TORTY
#### Requirements
Torty is designed to be easy-to-install, but there are a few requirements to get it up and running.

* PHP 7.2 or higher.
* MariaDB or MySQL.
* Apache or nginx web server.

It should work equally well when deployed on *BSD, Linux, macOS, or Windows operating systems.

#### Download
There are no official releases yet, but the latest version of Torty can be downloaded from [Codeberg](https://codeberg.org/10th-Street-Media/Torty).

#### Unpack
Unpack the ZIP or TAR.GZ file in your Downloads directory.

#### Deploy to server
Connect to your web server and copy the files from the Downloads folder to the web server.

#### Run installation script
Open a web browser and go to `https://your.webserver.url/index.php`. This should start the installation process. If it doesn't, try opening `https://your.webserver.url/dash/admin/torty-install.php`.

---
Torty English language documentation; Copyright 2020 by 10th Street Media, LLC and released under the [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).

If you are creating a project, plugin, theme, or translation for Torty, feel free to use these documents as a template to document the work you are doing.
