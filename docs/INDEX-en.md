# Torty Documentation
*This documentation applies to Torty v0.1*

All of the documentation for this version of Torty.
---

### The Basics
These documents cover the basics of running Torty.
* [Install Torty](INSTALL-en.md)
* [Configure Torty](CONFIGURE-en.md)
* [Upgrade Torty](UPGRADE-en.md)
* [Move Torty](MOVE-en.md)

### Hacking Torty
These documents explain how to hack Torty to use it in a project.

### The Details
These documents delve into Torty's code to detail functions, variables, files, and the database structure.
* [Torty files](FILES-en.md) - files included in Torty.
* [Torty functions](FUNCTIONS-en.md) - Functions included with Torty.
* [Torty cookies](COOKIES-en.md) - cookies set by Torty.

---
Torty English language documentation; Copyright 2020 by 10th Street Media, LLC and released under the [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).

If you are creating a project, plugin, theme, or translation for Torty, feel free to use these documents as a template to document the work you are doing.
