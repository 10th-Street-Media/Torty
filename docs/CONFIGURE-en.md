# Torty Documentation
*This documentation applies to Torty v0.1*

### CONFIGURING TORTY


#### During installation

#### After installation

---
Torty English language documentation; Copyright 2020 by 10th Street Media, LLC and released under the [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).

If you are creating a project, plugin, theme, or translation for Torty, feel free to use these documents as a template to document the work you are doing.
