# Icons needed for Torty, etc.
To move away from third-party libraries like FontAwesome, we'll need to find icons that can be included with Torty and other projects from 10th Street Media. The icons should be in the public domain or use a license that allows commercial distribution. If the icons can't be found, they'll need to be developed. All icons shoud be flat and black.

#### Menu items
* dashboard
* caret-right
* caret-left
* caret-up
* caret-down
* post
* page
* message
* contact
* contact-group
* company
* company-group
* form
* bookmark
* bookmark-group
* audio
* audio-group
* image
* image-group
* video
* video-group
* document
* document-group
* book
* book-group
* package
* package-group
* profile
* import-profile
* export-profile
* edit-profile
* avatar
* password
* security
* time
* time-zone
* calendar
* administrator
* theme
* plugin
* configure
* users
* suspend-user
* ban-user
* database
* database-table
* database-record
* moderator
* alert
* warning
* stop
* remove/delete
* translator
* locale

#### Messages
* like
* dislike
* boost
* comment
* flag

#### Posts
* category
* category-group
* tag
* tag-group
* collection
* collection-group
* schedule-post
* quotation

#### Pages
* main-page
* sub-page
* sub-pages

#### Coding
* framework
* PHP
* HTML
* CSS
* JS
* MariaDB
* Electron
* Windows
* Mac
* Linux
* BSD
* Plan-9 ;-)
* text-editor
* computer-code
* command-line
* WAI-ARIA
* accessibility

#### Platforms
* desktop-pc
* laptop
* tablet
* smartphone
* feature-phone
* TV
* radio
* office-phone
* web-camera

#### Media types
* SVG
* JPG
* PNG
* GIF
* MP3
* WAV
* OGG
* M4A
* MP4
* M4V
* WEBM
* ZIP
* TAR.GZ
* RAR
* DEB
* RPM
* MSI
* DMG
* CBZ
* CBR
* EPUB

#### Shopping
* shopping-cart
* shopping-bag
* dollar
* euro
* pound
* yen
* ruble
* crown
* peso
* baht
* shekel
* turkish-lira
* bitcoin
* litecoin
* ethereum
* billing-address
* shipping-address
* credit-card
* cash
* check
* free (gratis/no cost)

#### Miscellaneous
* search
* magnifying-glass
* zoom-in
* zoom-out
* photo-camera
* film-camera
* photo
* photo-group
* film
* film-group
* microphone
* musical-note
* musical-note-group
* treble-clef
* bass-clef
* swipe-left
* swipe-right
* sun
* moon
* comet
* star
* planet
* sneaker
* boot
* high-heels
* sandals
* car
* truck
* bus
* school-bus
* train
* subway/metro/light rail
* ship
* boat
* airplane
* helicopter
* blimp
* balloon
* party
* fireworks
* pine-tree
* season-spring
* season-summer
* season-autumn
* season-winter
* degrees-c
* degrees-f
* hot
* cold
* flame
* fire
* snowman
* snowflake
* raining
* fog
* windy
* cloudy
* partly-cloudy
* snowing
* no-smoking
* recycle-bin
* trash
* delete
* shred
* private-key
* public-key
* encrypt
* decrypt
* lock
* unlock
* fingerprint
* feed
* statistics

#### Brands
* Codeberg
* Github
* Gitlab
* Git
* Ubuntu
* Mint
* Fedora
* SuSE
* Gentoo
* Debian
* MX-Linux
* Manjaro
* elementary-OS
* Arch
* Solus
* Zorin
* Pop-OS
* CentOS
* Android
* iOS
* Google
* Chrome
* Firefox
* Amazon
* Microsoft
* Apple
* Facebook
* Instagram
* Signal
* Weibo
* Kik
* Twitter
* Etsy
* Ebay
* PayPal
* Stripe
* Patreon
* Liberapay
* Ko-Fi
* Open-Collective
* Mastodon
* Misskey
* Pixelfed
* Peertube
* Funkwhale
* Hubzilla
* diaspora
* Fediverse
* Pleroma
* Socialhome
* GnuSocial
* postActiv
* Friendica
* Plume
* WordPress
* Writefreely
* ICQ

#### 10th Street Media
* 10th-street-media
* torty
* federama
* pulse
* amore
* hobgoblin
* voynich
* zirkus
* cauldron
* sunbeam
* holdpass
* notas
* bao
* tunguska
* tempo
* lexadex