# Torty Documentation
*This documentation applies to Torty v0.1*

### FILES

All of the files included with this version of Torty.
---

#### conn.php
`conn.php` is created during the installation process. During the installation process, the user-supplied database information is used to create the `conn.php` file.

#### functions.php
`functions.php` is a central location for the functions in Torty.

#### docs/CONFIGURE-en.md
`CONFIGURE-en.md` is English language documentation that describes the configuration options for this version of Torty.

#### docs/COOKIES-en.md
`COOKIES-en.md` is English language documentation that describes the cookies set by this version of Torty.

#### docs/FILES-en.md
`FILES-en.md` is English language documentation that describes the files in this version of Torty.

#### docs/FUNCTIONS-en.md
`FUNCTIONS-en.md` is English language documentation that describes the functions available in this version of Torty.

#### docs/icons-needed.md
`icons-needed.md` is a list of icons that will be useful in Torty and other projects by 10th Street Media. The document is more of a TO-DO list than actual documentation.

#### docs/INDEX-en.md
`INDEX-en.md` is an index of the English language documentation included in this version of Torty.

#### docs/INSTALL-en.md
`INSTALL-en.md` is English language documentation for installation of this version of Torty.

#### docs/MOVE-en.md
`MOVE-en.md` is English language documentation that describes how to move this version of Torty from one server to another.

#### docs/UPGRADE-en.md
`UPGRADE-en.md` is English language documentation that describes how to upgrade from this version of Torty to the next.

#### keys/.htaccess
`.htaccess` in the `keys` folder is intended to keep people, bots, etc from indexing or accessing any of the files in this folder.

#### pub/index.html
`index.html` just redirects to `index.php` but it's also a good place for apps and websites to put a splash screen or logo.

#### pub/index.php
`index.php` is the main page for the website. It will check to see if a website exists. If it doesn't it will initiate the installation. If a website does exist, `index.php` will display a login or registration form.

#### pub/.well-known/.htaccess
`.htaccess` in the `.well-known` folder lets the server know what to do with the `nodeinfo` file that will be written in this folder.

#### pub/dash/delete-profile.php
`delete-profile.php` allows a user to delete their profile from the website.

#### pub/dash/edit-profile.php
`edit-profile.php` allows a user to edit their profile on the website.

#### pub/dash/footer.php
`footer.php` provides a footer for all pages in the Torty Dashboard.

#### pub/dash/header.php
`header.php` provides a header for all pages in the Torty Dashboard.

#### pub/dash/index.php
`index.php` in the `dash` folder is the main dashboard for logged in users.

#### pub/dash/nav.php
`nav.php` provides a navigation sidebar for all pages in the Torty Dashboard.

#### pub/dash/profile.php
`profile.php` displays a user's profile.

#### pub/dash/admin/add-user.php
`add-user.php` allows the website administrator to add a user.

#### pub/dash/admin/admin-header.php
`admin-header.php` provides a header for the installation page.

#### pub/dash/admin/admin-footer.php
`admin-footer.php` provides a footer for the installation pages.

#### pub/dash/admin/ban-user.php
`ban-user.php` allows a website administrator to ban a user.

#### pub/dash/admin/data-fill.php
`data-fill.php` is used during the installation process to fill some of the tables with default data.

#### pub/dash/admin/delete-user.php
`delete-user.php` allows the website administrator to delete a user.

#### pub/dash/admin/edit-user.php
`edit-user.php` allows a website administrator to edit a user's profile.

#### pub/dash/admin/final.php
`final.php` is the last step of the installation process.

#### pub/dash/admin/post-install.php
`post-install.php` is part of the installation process. After the tables have been created, the website administrator uses this page to give the site a name and create an admin account.

#### pub/dash/admin/schema.php
`schema.php` is part of the installation process. It is used to create the database and tables, and fill some of the tables with default data.

#### pub/dash/admin/suspend-user.php
`suspend-user.php` allows a website administrator to suspend a user for a period of time.

#### pub/dash/admin/torty-install.php
`torty-install.php` contains a form where a user will enter database information and credentials to start the installation-process.

#### pub/dash/admin/users.php
`users.php` is a list of the website's users. This is only viewable by the administrators.

#### pub/dash/images/favicon.ico
`favicon.ico` is the favicon for the Torty Dashboard.

#### pub/dash/style/dash-style-default.css
`dash-style-default.css` is the default stylesheet for the Torty Dashboard.

#### pub/dash/style/quill.bubble.css
`quill.bubble.css` is a stylesheet for the Quill Editor.

#### pub/dash/style/quill.core.css
`quill.core.css` is a stylesheet for the Quill Editor.

#### pub/dash/style/quill.core.js
`quill.core.js` is a script file for the Quill Editor.

#### pub/dash/style/quill.js
`quill.js` is a script file for the Quill Editor.

#### pub/dash/style/quill.min.js
`quill.min.js` is a script file for the Quill Editor.

#### pub/dash/style/quill.min.js.map
`quill.min.js.map` is a script file for the Quill Editor.

#### pub/dash/style/quill.snow.css
`quill.snow.css` is a stylesheet for the Quill Editor.

#### pub/images/favicon.ico
`favicon.ico` is the icon that will appear in the browser tab to represent the website. The included `favicon.ico` is a 48 x 48 pixels version of the Torty icon.

#### pub/images/torty-600.png
`torty-600.png` is a 600 pixel version of the Torty icon.

#### pub/images/torty-blk-600.png
`torty-blk-600.png` is a black Armenian capital letter Ech, which is used in the Torty icon because it resembles a stylized Latin small letter `t`.

#### pub/images/torty-blk-wht-600.png
`torty-blk-wht-600.png` is a version of the Torty icon with a black background and a white stylized `t` logo.

#### pub/images/torty-white-24.png
`torty-white-24.png` is a 24 pixel tall PNG icon that will appear at the top of many of the default pages included in Torty.

#### pub/images/torty-white-600.png
`torty-white-600.png` is a white Armenian capital letter Ech, which is used in the Torty icon because it resembles a stylized Latin small letter `t`.

#### pub/images/torty-wht-blk-600.png
`torty-wht-blk-600.png` is a version of the Torty icon with a white background and a black stylized `t` logo.

#### pub/includes/configuration-data.php
`configuration-data.php` retrieves data from the configuration table.

#### pub/includes/database-connect.php
`database-connect.php` connects Torty to the database.

#### pub/includes/nodeinfo.php
`nodeinfo.php` creates the files that gives Nodeinfo some information about this website.

#### pub/includes/ty-footer.php
`ty-footer.php` provides a default footer if the website has no theme. It is used by the login and registration pages.

#### pub/includes/ty-header.php
`ty-header.php` provides a default header if the website has no theme. It is used by the login and registration pages.

#### pud/includes/verify-cookies.php
`verify-cookies.php` checks the cookies and uses those to get the user information from the database. Probably not a very secure method.

#### pub/nodeinfo/.htaccess
`.htaccess` in the `pub/nodeinfo` folder defines the JSON files used by Nodeinfo.

#### pub/nodeinfo/version.php
`version.php` provides the software version as the constant VERSION.

#### pub/users/.htaccess
`.htaccess` in the `users` folder exists solely to make sure we don't have an empty folder. Other files will be written in this folder at some point.

---
Torty English language documentation; Copyright 2020 by 10th Street Media, LLC and released under the [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).

If you are creating a project, plugin, theme, or translation for Torty, feel free to use these documents as a template to document the work you are doing.
