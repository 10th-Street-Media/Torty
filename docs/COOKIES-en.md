# Torty Documentation
*This documentation applies to Torty v0.1*

### TORTY COOKIES


All cookies set by this version of Torty.
---

#### id
The `id` cookie has the user's id number. The `uname` cookie is preferred over `id`.


#### lingua
The `lingua` cookie sets the user's locale. It is called by dash/admin/admin-header.php.


#### loc
The `loc` cookie also sets the locale. This duplication will have to be fixed or explained.


#### nomen
The `nomen` cookie is the user's display name, but defaults to the username if the display name hasn't been set.


#### PHPSESSID
The `PHPSESSID` cookie is the PHP session id. This may be replaced by a cookie that combines the username and the date the session cookie was created.


#### uname
The `uname` cookie is set with the user's username.

---
Torty English language documentation; Copyright 2020 by 10th Street Media, LLC and released under the [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/).

If you are creating a project, plugin, theme, or translation for Torty, feel free to use these documents as a template to document the work you are doing.
