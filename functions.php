<?php
/**
 * PSR-2
 * functions.php
 *
 * This file is used to store nearly all functions used by Federama.
 *
 * since Federama version 0.1
 *
 */

include "conn.php";

$metadescription = _("<i>Torty</i> is an open-source PHP/MariaDB framework for the Fediverse - a decentralized social network of thousands of different communities and millions of users.");

// sanitizes text inputs from forms
function nicetext($text) {
    // get rid of whitespace characters at start or end of text
    $text = trim($text);

    // removes \ backslash escape characters
    $text = stripslashes($text);

    // converts special characters (i.e. < > &, etc) into their html entities
    #$text = htmlspecialchars($text,ENT_QUOTES,'UTF-8',true);

    $text = preg_replace('/\'/i', '&apos;', $text);
    return $text;
}


// a quicker way of decoding html entities
function retext($text) {
    $text = html_entity_decode($text,ENT_QUOTES,'UTF-8');

    return $text;
}


// turns a title into a url-friendly slug
function makeslug($text) {
    /**
     * taken from https://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes
     */

    // make all letters lowercase
    $text = strtolower($text);

    // make it alphanumeric and turn other characters into hyphens
    $text = preg_replace("/[^a-z0-9_\s-]/", "", $text);

    // clean up multiple hyphens and whitespaces
    $text = preg_replace("/[\s-]+/", " ", $text);

    // convert whitespaces and underscores to hyphens
    $text = preg_replace("/[\s_]/", "-", $text);

    return $text;
}

// turns a fileneame into something url-friendly, but keeps the . for the extension.
function makenicefile($text) {
    /**
     * taken from https://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes
     */

    // make all letters lowercase
    $text = strtolower($text);

    // make it alphanumeric and turn other characters into hyphens
    $text = preg_replace("/[^a-z0-9\.]/", "-", $text);

    // clean up multiple hyphens and whitespaces
    //$text = preg_replace("/[\s-]+/", " ", $text);

    // convert whitespaces and underscores to hyphens
    $text = preg_replace("/[\s_]/", "-", $text);

    return $text;
}


// finds @usernames and turns them into links
function profileparser($text) {

    // link will be something like <a href='http://example.tld/users/username'>@username</a>
    $newtext = preg_replace('/(@([a-zA-Z0-9]+))/i', '<a href=\''.$website_url.'users/\2\'>\1</a>',$text);
    return $newtext;

}

// displays a warning notice on a page
function warning_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class warning notice is for warnings -->\n";
    $ntc .= "\t<div class=\"warning-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays an error notice on a page
function error_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class error notice is for errors -->\n";
    $ntc .= "\t<div class=\"error-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays an important notice on a page
function important_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class important notice is for general messages & warnings -->\n";
    $ntc .= "\t<div class=\"important-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays a banned account notice on a page
function banned_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class banned notice is for banned users -->\n";
    $ntc .= "\t<div class=\"banned-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays a suspended account notice on a page
function suspended_notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class suspended notice is for suspended accounts -->\n";
    $ntc .= "\t<div class=\"suspended-notice\">".$notice."</div>\n";

    return $ntc;
}

// displays a generic notice on a page
function notice($notice) {
    $ntc = "\t<div class=\"clear\"></div>\n\n";
    $ntc .= "\t<!-- div class notice is for general notices & warnings -->\n";
    $ntc .= "\t<div class=\"notice\">".$notice."</div>\n";

    return $ntc;
}

// redirects to another page
function redirect($location) {
    return header("Location: $location");
}

// get the date of birth, return the age
function user_age($userage) {
    return floor((time() - strtotime($userage))/31556926);
}

// strip the protocol from a url
function short_url($url) {
    return preg_replace('/(https:\/\/)|(http:\/\/)/i', '', $url);
}

 // get the number of users
 function user_quantity($users) {
     $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
     $userqq = "SELECT * FROM ".TBLPREFIX."users";
     $userqquery = mysqli_query($dbconn,$userqq);
     $userqty = mysqli_num_rows($userqquery);

     return $userqty;
 }

 // get the number of active users over the past six months
function users_half_year($sometimes_users) {
    $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

    $usershalfyear = 0;

    $usershalfyearq = "SELECT * FROM ".TBLPREFIX."users";
    $usershalfyearquery = mysqli_query($dbconn,$usershalfyearq);
    while ($usershalfyearopt = mysqli_fetch_assoc($usershalfyearquery)) {
        $lastlogin  = strtotime($usershalfyearopt['user_last_login']);
        $now        = strtotime('now');
        if (($now - $lastlogin) < 15778800) { // 15778800 is six months in seconds
            $usershalfyear++;
        }
    }

    return $usershalfyear;
}

 // get the number of active users over the past month
 function users_past_month($active_users) {
     $dbconn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

     $usersmonthqty = 0;

     $usersmonthq = "SELECT * FROM ".TBLPREFIX."users";
     $usersmonthquery = mysqli_query($dbconn,$usersmonthq);
     while ($usersmonthopt = mysqli_fetch_assoc($usersmonthquery)) {
         $lastlogin = strtotime($usersmonthopt['user_last_login']);
         $now       = strtotime('now');
         if (($now - $lastlogin) < 2629800) { // 2629800 is one month in seconds
             $usersmonthqty++;
         }
     }

     return $usersmonthqty;
 }
